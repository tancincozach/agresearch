

var interval;


var global_config;


 var google_map_api = {

            markerGroups :[],
           infoWindow : new google.maps.InfoWindow(),


           getMarkerColor:function( group ){

            var color = '';
              $('.filter_group').each(function(){
                 

                  if($(this).val()==group){

                      color = $(this).attr('alt');

                  }
              });

              return color;
           },

           setMap:function (m) {
                      x = m.getZoom();
                      c = m.getCenter();
                      google.maps.event.trigger(m, 'resize');
                      m.setZoom(x);
                      m.setCenter(c);
                  },

            getAddress: function (latLng) {
                        $('#geo_address').remove();
                        geocoder = new google.maps.Geocoder();  
                        geocoder.geocode( {'latLng': latLng},
                          function(results, status) {
                     
                            if(status == google.maps.GeocoderStatus.OK) {
                              if(results[0]) {                          
                                                            
                                 $("<input type=\"hidden\"  id=\"geo_address\" value=\""+results[0].formatted_address +"\" >").appendTo('body');
                              }
                              else {                           
                                  $("<input type=\"hidden\"  id=\"geo_address\" value=\"No Result\" >").appendTo('body');
                              }
                            }
                            else {
                              $("<input type=\"hidden\"  id=\"geo_address\" value=\""+status+"\" >").appendTo('body');
                            }

                          });
                
                   }
                ,
                xmlParse:function(str){
                     if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
                                    var doc = new ActiveXObject('Microsoft.XMLDOM');
                                    doc.loadXML(str);
                                    return doc;
                                }

                                if (typeof DOMParser != 'undefined') {
                                    return (new DOMParser()).parseFromString(str, 'text/xml');
                                }

                                return createElement('div', null);
                }
                ,

                createMarker:function(point, name, type, map,html ,custom) {



                     var marker_object = {
                                map: map,
                                type: type ,
                                position: point,    
                                icon:custom.icon,                                                            
                                map_icon_label: custom.label.map_label
                               
                            }

                           var marker = new Marker(marker_object);


                            if (!google_map_api.markerGroups[type]) google_map_api.markerGroups[type] = [];
                            google_map_api.markerGroups[type].push(marker);

                            google_map_api.bindInfoWindow(marker, map, google_map_api.infoWindow, html);
                            return marker;

                },

                toggleGroup:function (type) {                  
                            for (var i = 0; i < google_map_api.markerGroups[type].length; i++) {
                                var marker = google_map_api.markerGroups[type][i];
                                if (!marker.getVisible()) {
                                    marker.setVisible(true);
                                    $('.'+type).show();

                                } else {
                                    marker.setVisible(false);
                                    $('.'+type).hide();
                                }
                            }
                },

                bindInfoWindow:function(marker, map, infoWindow, html){

                    isMobile = navigator.userAgent.match(/(iPhone|iPod|Android|BlackBerry|iPad|IEMobile|Opera Mini)/);



                    if(isMobile){


                            google.maps.event.addListener(marker, 'click', function () {
                                infoWindow.setContent(html);
                                infoWindow.open(map, marker);

                            });

                    }else{
                            google.maps.event.addListener(marker, 'mouseover', function () {
                                infoWindow.setContent(html);
                                infoWindow.open(map, marker);

                            });
               
                    }

                },

                initGoogleMap:function(json_data ,  map_container){     
                

                      try{


                          google_location_data = json_data.things_result;

                          default_geo_location = json_data.last_seen_thing;      


                           if($.isEmptyObject(default_geo_location.loc)) throw "ERROR: Empty Coordinates for Google Map Api";



                            var map = new google.maps.Map(map_container, {
                              zoom: 11,
                              center: new google.maps.LatLng(default_geo_location.loc.lat, default_geo_location.loc.lng),                              
                              mapTypeId: 'satellite'
                            });
                            

                             var marker;

                              $.each(google_location_data,function(i,google_info_details){


                                  var full_address =  '';


                                      full_address+='<div style="height:300px;overflow:auto"><table class="table table-bordered table-condensed danger" style="font-size:10px">';
                                      full_address+='<thead>';
                                      full_address+='<tr class="info"><th class="text-center">'+google_info_details.name+'</th></tr>';
                                      full_address+='</thead>';

                                  if(!$.isEmptyObject(google_info_details.alarms)){

                                            
                                                full_address+='<tr class="danger">';
                                                full_address+='<td >ALARMS</td>';
                                                full_address+='</tr>';
                                              $.each(google_info_details.alarms,function(alarm_name,alarm_values){

                                                full_address+='<tr >';
                                                full_address+='<td><label>&nbsp;&nbsp;-'+alarm_name+'</label></td>';
                                                full_address+='</tr>';

                                              });
                                              full_address+='</table>';

                                  }


                             full_address+='<table class="table table-bordered" style="font-size:10px">';
                                        full_address+='<thead>';
                                        full_address+='<tr class="warning"><th >PROPERTIES</th></tr>';
                                        full_address+='</thead>';

                                     var xml_marker = '<markers>';

                                         var icon ='' , type ='';

                                    $.each(google_info_details.properties,function(name,property){


                                      if (typeof property.name != 'undefined'){

                                          full_address+='<tr><td><label>'+property.name+':</label>&nbsp;'+property.value+' '+(typeof property.unit != 'undefined' ? property.unit:'')+'</td></tr>';
                                                    
                                      }

                                          xml_marker +='<marker name="'+google_info_details.id+'"  lat="'+google_info_details.loc.lat+'" lng="'+google_info_details.loc.lng+'"  type="'+(typeof google_info_details.tags[0] != 'undefined' || google_info_details.tags[0]=='' ? google_info_details.tags[0]:'Ungrouped Device(s)')+'" />';

                                    });

                           full_address+='</table>';

                                full_address+='<table class="table table-bordered" style="font-size:10px">';
                                  full_address+='<thead>';
                                  full_address+='<tr class="success"><th >COORDINATES</th></tr>';
                                    full_address+='</thead>';

                                     full_address+='<tr><td><label>Longitude: '+google_info_details.loc.lng+'</label> </td></tr>';
                                     full_address+='<tr><td><label>Latitude: '+google_info_details.loc.lat+'</label> </td></tr>';
                                     full_address+='<tr><td><label>Last Position: '+(typeof google_info_details.locUpdated != 'undefined' ? google_info_details.locUpdated:'')+'</label> </td></tr>';                                     
                                   

                                   full_address+='</table></div>';
                            
                                    xml_marker +='</markers>';                                    


                                    var xml = google_map_api.xmlParse(xml_marker) , markers = xml.documentElement.getElementsByTagName("marker") ,group_markers = [];

                                    

                                    for (var i = 0; i < markers.length; i++) {


                                        var name = markers[i].getAttribute("name");
                                        
                                        var address = markers[i].getAttribute("address");


                                        var type = markers[i].getAttribute("type");

                                        var point = new google.maps.LatLng(
                                        parseFloat(markers[i].getAttribute("lat")),
                                        parseFloat(markers[i].getAttribute("lng")));


                                        var html = "<b>" + name + "</b> <br/>" + address;


                                        var custom ={};

                                        var color = google_map_api.getMarkerColor(type);

                                         custom.icon =   {
                                                            path: MAP_PIN,
                                                            fillColor: color,                                       
                                                            fillOpacity: 1,
                                                            strokeColor: '',
                                                            strokeWeight: 0
                                                          };

                                       custom.label = { map_label: '<img  class="'+type+'"  style="margin-top:-45px" src="https://maxcdn.icons8.com/office/PNG/16/Animals/cow-16.png" title="Cow" width="32" height="32">' };
                                          


                                        var marker = google_map_api.createMarker(point, name, type, map,full_address,custom);

                                        group_markers.push( marker);
                                    }

                                     
                                       
                                  
                              }); 


                      
                                   

                  }catch( err ){

                         alert(err.message||err);

                  }
                       
          }            
  }





 chart  = {

		render_solid_graph:function(){


			

		},
		render_temp_graph:function(){

			

		},
		render_map_graph:function( json_object =''){
        

             thing_values  = (json_object !='' ? json_object:thing_result_object);
          

            google_map_api.initGoogleMap(thing_result_object ,  document.getElementById("map_canvas"));



		},
    render_sidebar_filter:function(json_object){

    }


}



 var main = {

        system_msg_template:function( msg){

            switch( msg ){

              case 'implement_config':

                return '<div class="col-md-12"><div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;Implementing configuration.Please wait.</div></div>';
              break;
              case 'loading':

                return '<div class="col-md-12"><div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;Gathering data. Please wait.</div></div>';
              break;

            }

        },

        disable_fields:function(){
            $('input[data-toggle=toggle]').bootstrapToggle('disable');
            $('input[name=input_time]').attr('disabled','disabled');
            $('select[name=time_type]').attr('disabled','disabled');
        },  

        enable_fields:function(){
          $('input[data-toggle=toggle]').bootstrapToggle('enable');
          $('input[name=input_time]').removeAttr('disabled');
          $('select[name=time_type]').removeAttr('disabled');
        },  

        convert_time_to_milliseconds:function(time_value,type){

          var milliseconds = '';            

            switch( type ){
              case 'min':

                    var seconds = parseInt(time_value) * 60;
                   milliseconds = parseInt(seconds) * 1000;
              break;

              case 'sec':

                    milliseconds = parseInt(time_value) * 1000;
              break;

            }

            return milliseconds;
        },

        html_entities:function (str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        },


        decode_html_entities: function  (str) {
            if(str && typeof str === 'string') {
              // strip script/html tags
              str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
              str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            
            }

            return str;
          },

    		auto_load:function( start ){

    			if(start==false) {              



    			      this.exec_rest_interval();      

    			  }else{

    			    clearInterval(interval);     

               $('#auto-load-switch').bootstrapToggle('on'); 
                     
                      

    			  }

    		},

        load_configuration:function(){

          $.ajax({
                url:'ajax/get_config',
                beforeSend: function(jqXHR, settings) {
                  
                    main.disable_fields();

                    $('#config-msg').html(main.system_msg_template('implement_config'));


                },
                success:function(json_object){

                    main.enable_fields();


                  $('input[data-toggle=toggle]').bootstrapToggle('enable');



                   if(! $.isEmptyObject(json_object) && json_object.length > 0){



                        

                        var json_result = JSON.parse(json_object);
                        
                          

                        if(! $.isEmptyObject(json_result.interval)){
                              $('input[name=input_time]').val(json_result.interval);                           

                        }


                        if(! $.isEmptyObject(json_result.time_type)){
                               $('select[name=time_type]').val(json_result.time_type);
                        }


                        global_config = json_result;

                          $('#config-msg').html('');

                   }
                   
                }
          });
        }
        ,

		   exec_rest_interval : function( propType ){

		   	    var interval_time = $('input[name=input_time]').val() , time_type_value = $('select[name=time_type]').val(), time_limit_request = 300000;


            var interval_time_ms = main.convert_time_to_milliseconds(interval_time , time_type_value);

		   	    if(interval_time_ms!=''){

                  $.ajax( {url:'ajax/implement_configuration',                      
                                type:'POST',
                                beforeSend: function(jqXHR, settings) {
                                
                                    $('#system-msg').html(main.system_msg_template('loading'));
                                    if( settings.type == 'POST' ){ 
                                        settings.data = settings.data+'&'+smart_cow_global.n+'='+smart_cow_global.h;
                                         main.disable_fields();
                                    }
                                    return true;
                                },
                                data:{interval:interval_time,time_type:time_type_value},
                                success:function( JsonObj ) {     
                               
                                   $('#system-msg').html('');

                                 if( $.isEmptyObject(JsonObj.error)){

                                    global_config = JsonObj.config;

                                      var ajax_request_object =  main.fetch_things();

                                      console.log(interval_time_ms);

                                        interval = setInterval(
                                                    function()
                                                     {
                                                        console.log(interval_time_ms);
                                                          ajax_request_object.done(function(){
                                                              main.fetch_things();                                   
                                                         }); 
                                                      },
                                          interval_time_ms);  

                                    setTimeout(function() {

                                            main.auto_load(true);

          
                                       
                                       }, time_limit_request);   
                                 }                                                         

                                },
                                error:function( errorCode ){
         
                                
                                }
                                ,
                                dataType:'json'
                  });
                              

            }            
       
        },  

		implement_settings:function(){


           $('#auto-load-switch').bootstrapToggle('on'); 

           $('#auto-load-switch').change(function(event) {

            var checked = $(this).prop('checked');

            if(typeof checked != 'undefined' ||  checked.legnth > 0){

               if( $(this).prop('checked')==false && parseInt($('input[name=input_time]').val()) < 5 &&  $('select[name=time_type]').val()=='sec'){

                
                  $(this).bootstrapToggle('on');  
                
                  alert('Please input more or equal to 5 seconds.');

                  $( "input[name=input_time]" ).focus();
                   

                 return false;
              }
            }

              

              
		         		main.auto_load($(this).prop('checked'));

                 event.preventDefault();
				      
			     });


           $('.filter_group').on('click',function(){ 

              
                google_map_api.toggleGroup($(this).val());
              

           });

          
		},

		fetch_things:function(){
			
    
		   return $.ajax({ 
                        url:'ajax/get_things',                      
                        type:'POST',             
                        data:{controller:'things'},           
                        success:function( JsonObj ) {               
                      
                          main.enable_fields();

                          $('#system-msg').html('');

                          if( $.isEmptyObject(JsonObj.error)){
                            chart.render_map_graph(JsonObj);                            

                          }else{       
                            
                          }
                        

                        },
                        error:function( errorCode ){
                      
          
                        },
                        dataType:'json'
                      });


		},


    post_settings:function( $post){
          $.ajax( {url:'ajax/implement_configuration',                      
                        type:'POST',
                        data:$post,
                        beforeSend: function(jqXHR, settings) {

                        settings.data = settings.data+'&'+smart_cow_global.n+'='+smart_cow_global.h;
                        $('input[data-toggle=toggle]').bootstrapToggle('disable');                                  

                         return true;
                        },
                        success:function( JsonObj ) {               

                          

                        },
                        error:function( errorCode ){
 
          
                        }
                        ,
                        dataType:'json'
          });
    },

		init:function(){	
        main.implement_settings();
        main.load_configuration();
					

		}
};

//This handles the queues    
(function($) {

  var ajaxQueue = $({});

  $.ajaxQueue = function(ajaxOpts) {

    var oldComplete = ajaxOpts.complete;

    ajaxQueue.queue(function(next) {

      ajaxOpts.complete = function() {
        if (oldComplete) oldComplete.apply(this, arguments);

        next();
      };

      $.ajax(ajaxOpts);
    });
  };

}

)(jQuery);



$( document ).ready(function(){

        $.ajaxSetup({
                beforeSend: function(jqXHR, settings) {
                

                    if( settings.type == 'POST' ){ 
                        settings.data = settings.data+'&'+smart_cow_global.n+'='+smart_cow_global.h;
                        $('input[data-toggle=toggle]').bootstrapToggle('disable');
                        $('input[name=input_time]').attr('disabled','disabled');
                         
                        $('#system-msg').html(main.system_msg_template('loading'));
                    }
                    return true;
                }
            });
         
                  main.init();


              $('[data-toggle=collapse]').click(function(){

                $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");


            });

              
                    $('.open_pane').click(function(event){
                        $('#mySidenav').css('width', '250px');
                        $('#main').css('margin-left', '250px');                    
                        event.preventDefault();
                    });
                
                    $('.close_pane').click(function(event){
                         $('#mySidenav').css('width', '0');
                        $('#main').css('margin-left', '0');                   
                        event.preventDefault();
                    });  

});






