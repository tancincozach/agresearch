
var misc = {



    count_selected_sel_box_graph:function( sel_box_object ){

       var checked_sel_box = [];

      $(sel_box_object).each(function(){          
            if($(this).is(':checked')){
                checked_sel_box.push($(this).val());
            }            
        });

         return checked_sel_box;
    }

}
var setup = {

   generate_report:function(){


      sel_box_values =   $('select[name=thing_graph_drowpdown]').val();

    if(typeof sel_box_values === 'undefined' || !sel_box_values){

            alert('Please select thing(s).');
          
            return false;

        }else{

          var date_rang_val = $('input[name=daterange]').val();   


             $('button[name=btn_gen_report]').prop("disabled", true).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Generating Report...');

              $.ajax({
                  url:'ajax/generate_report',
                  data:{sel_box:sel_box_values,daterange:date_rang_val},
                  type:'post',
                  success:function(result){

                    $('button[name=btn_gen_report]').prop("disabled", false).html('Generate Report');



                  
                 if(!$.isEmptyObject(result.error)){

                       alert(result.error);

                    }else{

                      window.location = "download/csv/?url="+result.url+"";
                        //window.open(result.url,'_blank' );

                    }                    
                    
                  },

                  dataType:'json',

                  error:function( error ){
                   //     alert(error);
                  }
              });

        }
   },

   soil_moisture_graph:function(){


   // var  sel_box_values = misc.count_selected_sel_box_graph('.soil_moisture_sel_box');
   // 
   // 

    sel_box_values =   $('select[name=thing_graph_drowpdown]').val();

    if(typeof sel_box_values === 'undefined' || !sel_box_values){


        alert('Cannot find thing(s).');
        return false;

    }else{


              $('button[name=bld_graph]').prop("disabled", true).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Generating Chart...');

          var date_rang_val = $('input[name=daterange]').val();  

          if(date_rang_val==''){

            alert('Please dont leave the date range empty.');
            return false;

          }else{

            $("form[name=report_form]").submit(function(e){

               e.preventDefault();

             });

                $.ajax({
              url:'ajax/json_graph',
              type:'post',
              data:{sel_box:sel_box_values.join(','),daterange:date_rang_val},           
              success:function( result ){

                $('button[name=bld_graph]').prop("disabled", false).html(' Generate Chart');

                     if(!$.isEmptyObject(result)){

                        var data_result_graph = [] ;


                        $('ul.thing_counter').html('');

                        $.each(result , function(i,thing_value){


                          var swc_history = [];

                          swc_history= [];  

                          $.each(thing_value.properties.swc_history,function(i2,swc_val){
                           
                                var date_split = (swc_val.new_date ? swc_val.new_date.split('-'):''), 

                                new_date =  new Date(swc_val.new_date);

                              
                                    swc_history.push({ x: new_date, y: swc_val.value });                 

                                
                        });       


                          $('ul.thing_counter').append('<li class="list-group-item"><h4>'+thing_value.name+' : <span class="label label-success" stype="padding:5px;">'+swc_history.length+'</span></h4></li>');



                            data_result_graph.push({
                                  type:"line",
                                  showInLegend: true,
                                  markerType: "circle",
                                  markerType: "circle",
                                  xValueFormatString:'MMM DD, YYYY HH:mm:ss',
                                  name: thing_value.name,
                                  dataPoints: swc_history
                                });
                 

                        });

                          var chart = new CanvasJS.Chart("soil_moisture_line_graph", {
                            animationEnabled: true,
                            zoomEnabled: true,
                            title: {
                              text: 'Soil Moisture',
                            },
                            axisX: {
                              valueFormatString: "MMM YYYY",
                              crosshair: {
                                enabled: true,
                                //snapToDataPoint: true
                              }
                            },
                            axisY2: {
                              text: 'Soil Moisture',
                              prefix: "",
                              suffix: "",
                              crosshair: {
                                enabled: true
                              }
                            },
                            toolTip: {
                              shared: true
                            },
                            legend: {
                              cursor: "pointer",
                              verticalAlign: "top",
                              horizontalAlign: "center",
                              dockInsidePlotArea: true,
                              itemclick: setup.toogleDataSeries
                            },
                            data:data_result_graph
                          });

                          chart.render();

                     }
              },
              dataType:'json'
            });
          }
     

      }


   },

   show_total_records:function(){

   },

 toogleDataSeries:function(e){
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
          e.dataSeries.visible = false;
        } else{
          e.dataSeries.visible = true;
        }
        e.chart.render();
     }
   ,

    submit :function(posted_values){

      $('button[name=submit_zone]').prop("disabled", true).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Submitting Paddock/Zone Settings values...');

      $.ajax({
        url:'ajax/submit_zone_settings',
        type:'post',
        beforeSend:function(){

        	$('table.setup_table > tbody > tr > td > button').prop("disabled", true);

        },
        data:posted_values,
        success:function(){
          $('button[name=submit_zone]').prop("disabled", false).html('Submit');
          setup.load_setup_table();
          setup.load_results();
          setup.load_application_time();
        }

      })

    },

   submit_pod :function(posted_values){

      $('button[name=submit_pod]').prop("disabled", true).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Submitting Pod values...');

      $.ajax({
        url:'ajax/submit_pod',
        type:'post',
        data:posted_values,
        beforeSend:function(){
        	$('table.pod_tbl > tbody > tr > td > button').prop("disabled", true);
        },
        success:function(){
          $('button[name=submit_pod]').prop("disabled", false).html('Submit');
            setup.load_setup_pod_table();
            setup.load_results();
            setup.load_application_time();
        },
        dataType:'json'
      })

    },


  submit_factor :function(posted_values){

      $('button[name=factor_btn]').prop("disabled", true).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Submitting Factors values...');
      
      $.ajax({
        url:'ajax/submit_factor_settings',
        type:'post',
        data:posted_values,
        success:function(response){
          $('button[name=factor_btn]').prop("disabled", false).html('Submit');
        },
        error:function( err){
      

        }
      });

    },

    fill_setup_info:function( obj_button ){

            var parent_row     =  $(obj_button).parent().parent() ;

            $('table.setup_table > tbody > tr').removeClass( "info" );


             parent_row .addClass( "info" );


  

            var pod_num_txt        =  parent_row.find('td.padlock_num').text(),
                zone_num_txt       =  parent_row.find('td.zone').text(),
                returning_days_txt =  parent_row.find('td.returning_days').text(),
                wp_value_txt       =  parent_row.find('td.wp_value').text(),
                buffer_txt         =  parent_row.find('td.buffer').text(),
                soil_type_val      =  parent_row.find('td.soil_type > input[type=hidden]').val(),
                irrigation_val      =  parent_row.find('td.irrigation_type > input[type=hidden]').val(),
                fiel_capacity_txt  =  parent_row.find('td.field_capacity').text();


               $('input[name=pod_num]').val((pod_num_txt=='N/A' || pod_num_txt=='' ? '':pod_num_txt));


               $('input[name=zone_num]').val((zone_num_txt=='N/A' || zone_num_txt=='' ? '':zone_num_txt));

               $('input[name=returning_days]').val((returning_days_txt=='N/A' || returning_days_txt=='' ? '':returning_days_txt));

               $('input[name=wp_value]').val((wp_value_txt=='N/A' || wp_value_txt=='' ? '':wp_value_txt));


               $('input[name=buffer]').val((buffer_txt=='N/A' || buffer_txt=='' ? '':buffer_txt));


              var irrigation_txt     =  parent_row.find('td.irrigation_type').text();

            

                

                 
              $('select[name=soil_type]').attr('disabled',false);  


               if(soil_type_val!=''){              

                  $('select[name=soil_type]').val(parseInt(soil_type_val)).change();

               }

                if(irrigation_val==0){
                  
                     $('select[name=soil_type]').val(0).change();   


                    $('select[name=soil_type]').attr('disabled',true);               
                    
                }

               
  
               $('input[name=irrigation_type').each(function(){      

                      $(this).prop('checked',false);

                        if(parseInt($(this).val())==irrigation_val){

                            $(this).prop('checked',true);
                        }


               });


               $('input[name=field_capacity]').val((fiel_capacity_txt=='N/A' || fiel_capacity_txt=='' ? '':fiel_capacity_txt));


    },

      fill_pod_info:function( obj_button ){

            var parent_row     =  $(obj_button).parent().parent() ;
         
                $('table.pod_tbl > tbody > tr').removeClass( "info" );

              parent_row .addClass( "info" );

            var pod_no        =  parent_row.find('td.pod_no').text(),
                ssid       =  parent_row.find('td.ssid').text(),
                adrs =  parent_row.find('td.adrs').text(),
                pswd       =  parent_row.find('td.pswd').text(),
                ap_rate         =  parent_row.find('td.ap_rate').text(),
                thing       =  $(obj_button).attr('alt');           

               $('input[name=pod_no]').val((pod_no=='N/A' || pod_no=='' ? '':pod_no));

               $('input[name=ssid]').val((ssid=='N/A' || ssid=='' ? '':ssid));

               $('input[name=adrs]').val((adrs=='N/A' || adrs=='' ? '':adrs));

               $('input[name=pswd]').val((pswd=='N/A' || pswd=='' ? '':pswd));

               $('input[name=ap_rate]').val((ap_rate=='N/A' || ap_rate=='' ? '':ap_rate));               

           		$('button[name=submit_pod]').attr('alt',thing);

    },
    init:function(){

              var hash = window.location.hash;
              $('#myTab a[href="' + hash + '"]').tab('show');

                $('.edit_zone_info').on('click',function(event){              
                      setup.fill_setup_info($(this));
                });

                  $('.edit_pod_info').on('click',function(event){              
                      setup.fill_pod_info($(this));
                });

           
              $( "#setup_form" ).submit(function( event ) {   

                  var post_values =  $(this).serializeArray();

                  if(!post_values.hasOwnProperty('soil_type')){

                        post_values.push({name:'soil_type',value:0});

                    }


                    if($('input[name=pod_num]').val()!='' || 
                    $('input[name=zone_num]').val()!='' || 
                    $('input[name=returing_days]').val()!='' || 
                    $('input[name=wp_value]').val()!='' || 
                    $('input[name=buffer]').val()!='' || 
                    $('input[name=irrigation_type]').val()!='' || 
                    $('input[name=field_capacity]').val()!=''){

                    setup.submit(post_values); 

                  }else{
                    alert('Please dont leave the input fields blank.');
                  }

                  event.preventDefault();
                  return false;
              });


              $( "#pod_form" ).submit(function( event ) {   

        			var post_values =  $(this).serializeArray();

        			var thing = $('button[name=submit_pod]').attr('alt');


        			post_values.push({name:'thing_key',value:thing});


                    if($('input[name=pod_no]').val()!='' || 
                    $('input[name=ssid]').val()!='' || 
                    $('input[name=adrs]').val()!='' || 
                    $('input[name=pswd]').val()!='' || 
                    $('input[name=ap_rate]').val()!=''){

                    setup.submit_pod(post_values); 

                  }else{
                    alert('Please dont leave the input fields blank.');
                  }

                  event.preventDefault();
                  return false;
              });

              $( "#soil_moisture_sensor_factor" ).submit(function( event ) {   
                 

              var post_values = $(this).serializeArray();


                post_values.push({name: 'thing_key', value: $('select[name=factor_sel]').val()});
             
                  if(post_values.length > 0 ){

                    setup.submit_factor(post_values); 

                  }else{
                    alert('Please dont leave the input fields blank.');
                  }

                  event.preventDefault();
                  return false;
              });

              $('.check_all_things').on('change',function(){

                var checked=false;


                $('button[name=bld_graph],button[name=btn_gen_report]').attr('disabled','disabled');

                  if($(this).is(':checked')){
                    
                    checked=true;

                    $('button[name=bld_graph],button[name=btn_gen_report]').removeAttr('disabled');

                  }
                    $('.soil_moisture_sel_box').each(function(){
                        $(this).prop("checked", checked);
                    });

              });

              $('.soil_moisture_sel_box').change(function(){

                  var  sel_box_values = misc.count_selected_sel_box_graph('.soil_moisture_sel_box');


                  if(sel_box_values.length > 0 ){

                     $('button[name=bld_graph],button[name=btn_gen_report]').removeAttr('disabled');

                   }else{
                    $('button[name=bld_graph],button[name=btn_gen_report]').attr('disabled','disabled');
                   }


              });

              $('input[name="daterange"]').daterangepicker();

                    fakewaffle.responsiveTabs(['xs', 'sm']);

                  $('.selectpicker').selectpicker({
                    style: 'btn-info',
                    size: 10
                  });


                $('select[name=factor_sel]').change(function(){

                    if($(this).val()!=''){
                        setup.load_factor_table();                        
                      }
                    
                });


                $('input[name=irrigation_type').click(function(){


                  console.log($(this).val());

                  

                      if(parseInt($(this).val())==0  ){

                          $('select[name=soil_type]').val(0).change();   

                        $('select[name=soil_type]').attr('disabled',true);

                      }else{

                       $('select[name=soil_type]').attr('disabled',false);
                      }

                });

    },

    load_setup_table:function(){
        $.ajax({
        url:'ajax/get_things',
        type:'get',
         beforeSend: function() {

                $('.msg').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Updating data to be displayed on Paddock/Zone table.</strong></div>');
                $('table.setup_table > tbody > tr > td > button').prop("disabled", true);
         },
        success:function( result){

          $('.msg').html('');

          $('table.setup_table > tbody > tr > td > button').prop("disabled", false);
            
            if(!$.isEmptyObject(result.things_zone_result)){

              var str_table = '';

              $.each(result.things_zone_result ,function(i,val){

                  var active = '';

              	if( $('button[name=submit_zone]').attr('alt')==val.name ){

              		 	 active = ' class="info" ';
              	}


                  str_table+='<tr '+active+'>'+
                                  '<td class="padlock_num">'+(typeof val.name!= 'undefined' ?  val.name:'N/A')+'</td>'+
                                  '<td class="zone">'+(typeof val.properties.zone_num != 'undefined' ?  val.properties.zone_num.value:'N/A')+'</td>'+
                                  '<td class="returning_days">'+(typeof val.properties.returning_days!= 'undefined' ? val.properties.returning_days.value:'N/A')+'</td>'+                                  
                                  '<td class="wp_value">'+(typeof val.properties.wp_value != 'undefined' ?  val.properties.wp_value.value:'N/A')+'</td>'+
                                  '<td class="buffer">'+(typeof val.properties.buffer != 'undefined' ? val.properties.buffer.value:'N/A')+'</td>'+
                                  '<td class="irrigation">'+(typeof val.properties.irrigation_type!= 'undefined' ?  val.properties.irrigation_type.value:'N/A')+'</td>'+
                                  '<td class="soil_type">'+(typeof val.properties.soil_type!= 'undefined' ? val.properties.soil_type.value:'N/A')+'</td>'+
                                  '<td class="field_capacity">'+(typeof val.properties.field_capacity!= 'undefined' ? val.properties.field_capacity.value:'N/A')+'</td>'+
                                  '<td class="depth">'+(typeof val.properties.depth != 'undefined' ? val.properties.depth.value:'N/A')+'</td>'+
                                  '<td class="mpda">'+(typeof val.properties.mpda != 'undefined' ?  val.properties.mpda.value:'N/A')+'</td>'+
                                  '<td><button class="btn btn-primary edit_zone_info">Edit</button></td>'+
                            '<tr/>';
              });

              if(str_table!=''){

                  $('table.setup_table > tbody').html( str_table );
              }

              $('.edit_zone_info').on('click',function(event){              
              	setup.fill_setup_info($(this));
      		  });

            }
      
        },
        dataType:'json'
      })

    }
    ,

      load_setup_pod_table:function(){
        $.ajax({
        url:'ajax/get_things',
        type:'get',
         beforeSend: function() {

                $('.msg_pod').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span>Updating data to be displayed on POD table.</strong></div>');
         },
        success:function( result){

          $('.msg_pod').html('');

          $('table.pod_tbl > tbody > tr > td > button').prop("disabled", false);
            
            if(!$.isEmptyObject(result.things_zone_result)){


              var str_table = '';

              $.each(result.things_zone_result ,function(i,val){

              	var active = '';

              	if( $('button[name=submit_pod]').attr('alt')==val.name ){

              		 	 active = ' class="info" ';
              	}


                  str_table+='<tr '+active+'>'+
                                  '<td class="padlock_num">'+((!$.isEmptyObject(val.attrs) &&  !$.isEmptyObject(val.attrs.pod_no)) && typeof val.attrs.pod_no.value!= 'undefined' ?  val.attrs.pod_no.value:'N/A')+'</td>'+
                                  '<td class="ssid">'+((!$.isEmptyObject(val.attrs) && !$.isEmptyObject(val.attrs.ssid)) && typeof val.attrs.ssid.value != 'undefined' ?  val.attrs.ssid.value:'N/A')+'</td>'+
                                  '<td class="adrs">'+((!$.isEmptyObject(val.attrs) && !$.isEmptyObject(val.attrs.adrs)) && typeof val.attrs.adrs.value!= 'undefined' ? val.attrs.adrs.value:'N/A')+'</td>'+                                  
                                  '<td class="pswd">'+((!$.isEmptyObject(val.attrs) && !$.isEmptyObject(val.attrs.pswd)) && typeof val.attrs.pswd.value != 'undefined' ?  val.attrs.pswd.value:'N/A')+'</td>'+
                                  '<td class="ap_rate">'+((!$.isEmptyObject(val.attrs) && !$.isEmptyObject(val.attrs.ap_rate)) && typeof val.attrs.ap_rate.value != 'undefined' ? val.attrs.ap_rate.value:'N/A')+'</td>'+
                                  '<td><button class="btn btn-primary edit_pod_info" alt="'+val.name+'">Edit</button></td>'+
                            '<tr/>';
              });

              if(str_table!=''){

                  $('table.pod_tbl > tbody').html( str_table );
              }


	          $('.edit_pod_info').on('click',function(event){              
		              setup.fill_pod_info($(this));
		        });


            }
      
        },
        dataType:'json'
      });

    },
    load_soil_moisture:function(){


       $.ajax({
        url:'ajax/soil_moisture',
        type:'get',
         beforeSend: function() {

               
         },
        success:function( result){



        }
      });
    },
    load_application_time:function(){


       $.ajax({
        url:'ajax/applicaction_time',
        type:'get',
         beforeSend: function() {

                $('.at_msg').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Updating data to be displayed on Application Time table.</strong></div>');
         },
        success:function( result){

          $('.at_msg').html('');
          $('#application_time').html(result);


        }
      });
    },
      load_results:function(){


       $.ajax({
        url:'ajax/results',
        type:'get',
         beforeSend: function() {

                $('.results').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Updating data to be displayed on Results table.</strong></div>');
         },
        success:function( result){

          $('.results').html('');
          $('#results').html(result);


        }
      });
    },
    load_factor_table:function(){

    $('#factor_form').html('<div class="alert alert-success"><strong><span class="glyphicon glyphicon-refresh glyphicon-refresh-spin"></span> Updating data to be displayed on Factor table.</strong></div>');
        $.ajax({
        url:'ajax/get_factor',
        data:{thing:$('select[name=factor_sel]').val()},
        type:'post',        
        datatype:'json',
         
        success:function( result){

          
            
            if(!$.isEmptyObject(result[0])){

            
            var str_table = '<table class="table table-bordered  table-condensed factor_table">'
                          + '<thead>'
                          +'<tr>'
                          +'<th>Factors</th>'
                          + '<th>Value</th> '              
                        + '</tr>'
                      +'</thead><tbody>';
              
              


              $.each(result[0].properties ,function(i,val){

                        var factors_arr = result.factors.split(',');

                       // console.log(factors_arr);


                        if($.inArray( i ,factors_arr) != -1) {


                                        str_table+='<tr>'+
                                                '<td>'+i+'</td>'+
                                                '<td><input type="text"  name="'+i+'" class="form-control"  value="'+result[0].properties[i].value+'"> </td>'+
                                              '</tr>';

                        }
                                   

              });


              str_table+='<tr>'
                          +'<td colspan="2" style="text-align:left !important;padding-left:32px;" >'
                          +'<button name="factor_btn" class="btn btn-primary">Save</button>'
                          +'</td>'
                      +'</tr>'
                +'</tbody>'
                +'</table>';
                
            $('#factor_form').html(str_table);

            }
      
        },
        dataType:'json'
      })

    }
}

$(function(){
 $.ajaxSetup({
        beforeSend: function(jqXHR, settings) {
             
            if( settings.type == 'POST' ){ 
                settings.data = settings.data+'&'+agresearch_global.n+'='+agresearch_global.h;
            }
            return true;
        }
    });
    setup.init();
});
