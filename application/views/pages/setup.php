<style>
	
	.bootstrap-select>.dropdown-toggle.bs-placeholder,.bootstrap-select>.dropdown-toggle.bs-placeholder:active,.bootstrap-select>.dropdown-toggle.bs-placeholder:focus,.bootstrap-select>.dropdown-toggle.bs-placeholder:hover {
 color:white;
}
</style>
    <div > 
      <h3>Setup</h3>
      <p >


        <ul id="setup_tab" class="nav nav-tabs responsive bar_tabs">
            <li role="presentation" class="active"><a href="#pod" id="home-tab" class="setup_tab" role="tab" data-toggle="tab" aria-expanded="true">Pods</a>
            </li>
            <li role="presentation" class=""><a href="#poddock" role="tab" class="setup_tab"   data-toggle="tab" aria-expanded="false">Paddock/Zone</a>
            </li>
            <li role="presentation" class=""><a href="#soil_sensor_factor" role="tab" class="setup_tab"  data-toggle="tab" aria-expanded="false">Soil Moisture Sensor Factors</a>                                            
            </li>
        </ul>


        <div id="setup_content" class="tab-content responsive">
            <div role="tabpanel" class="tab-pane fade in active" id="pod">
           
                	<h3>Pod</h3><br/>
        		  <?php echo  form_open('', ' id="pod_form"');?>

					  	<div class="row">
					  		<div class="col-md-6">
					  				    <div class="form-group">
										    <label for="pod_num">POD Number:</label>
										    <input type="text" class="form-control " id="pod_no" name="pod_no" >
										  </div>
										  <div class="form-group">
										    <label for="zone_num">SSID:</label>
										    <input type="text" class="form-control " id="ssid" name="ssid">
										  </div>
										  <div class="form-group">
										    <label for="returing_days">IP Address:</label>
										    <input type="text" class="form-control " id="adrs" name="adrs">
										  </div>
										
					  		</div>
					  		<div class="col-md-6">
					  			  <div class="form-group">
										    <label for="wp_value">Password:</label>
										    <input type="text" class="form-control " id="pswd" name="pswd">
										  </div>
										  <div class="form-group">
										    <label for="wp_value">Application Rate(mm/hr):</label>
										    <input type="text" class="form-control " id="ap_rate" name="ap_rate">
										  </div>
					  		</div>
					  	</div> 	
				
				
						  <button type="submit" class="btn btn-primary" name="submit_pod">Save</button><div class="msg_pod"></div>
        		  <?php echo  form_close();?>
        		  <div class="table-responsive">
        		  	<table class="table table-bordered  table-condensed  pod_tbl">
			  		<thead>
			  			<tr>
			  				<th>POD Number</th>
			  				<th>SSID</th>
			  				<th>IP Address</th>
			  				<th>Password</th>
			  				<th>Application Rate(mm/hr)</th>
			  				<th>Option</th>
			  			</tr>
			  		</thead>
			  			<tbody>
			  				<?php  foreach ($things_zone_result as $key => $thing):


			  				?>
		  					<tr>
			  					<td class="pod_no"><?php echo @$thing['attrs']['pod_no']['value'];?></td>
			  					<td class="ssid"><?php echo @$thing['attrs']['ssid']['value'];?></td>
			  					<td class="adrs"><?php echo @$thing['attrs']['adrs']['value'];?></td>
			  					<td class="pswd"><?php echo @$thing['attrs']['pswd']['value'];?></td>
			  					<td class="ap_rate"><?php echo @$thing['attrs']['ap_rate']['value'];?></td>
			  					<td><button class="btn btn-primary edit_pod_info" alt="<?php echo @$thing['name'];?>">Edit</button></td>
			  				</tr>
	  						<?php endforeach;?>	
			  			</tbody>
			  		</table>
        		  </div>
			  		

            </div>
            <div role="tabpanel" class="tab-pane fade" id="poddock" >

		  			<h3>Paddock/Zone Settings</h3><br/>
					  <?php echo  form_open('', ' id="setup_form" class="form-horizontal"');?>

					  	<div class="row">
					  		<div class="col-md-6">
					  				    <div class="form-group">
										    <label for="pod_num">Paddock Number:</label>
										    <input type="text" class="form-control " id="pod_num" name="pod_num" readonly="readonly">
										  </div>
										  <div class="form-group">
										    <label for="zone_num">Zone Number:</label>
										    <input type="text" class="form-control " id="zone_num" name="zone_num">
										  </div>
										  <div class="form-group">
										    <label for="returing_days">Returning Days:</label>
										    <input type="text" class="form-control " id="returning_days" name="returning_days">
										  </div>
										  <div class="form-group">
										    <label for="wp_value">WP Value:</label>
										    <input type="text" class="form-control " id="wp_value" name="wp_value">
										  </div>
					  		</div>
					  		<div class="col-md-6">
	  						  <div class="form-group">
						    		<label for="wp_value">Buffer:</label>
						    		<input type="text" class="form-control " id="buffer" name="buffer">
						  	</div>  
						  	<div class="form-group">
						    		<label for="irrigation_type">Irrigation Type:</label>
						    		<div class="radio">
									  <label><input type="radio" name="irrigation_type" value="0">Water</label>
									</div>
									<div class="radio">
									  <label><input type="radio" name="irrigation_type" value="1">Effluent</label>
									</div>
						    		
						  	</div>
						   <div class="form-group">
						    <label for="soil_type">Soil Type:</label>
						    <select class="form-control" id="soil_type" name="soil_type">
						    	<option value="0">None</option>
						    	<option value="1">Low Risk</option>
						    	<option value="2">High Risk</option>
						    	
						    </select>
						  </div>
						   <div class="form-group">
						    <label for="soil_type">Field Capacity:</label>
						    <input type="text" class="form-control " id="buffer" name="field_capacity">
						  </div>
					  		</div>
					  	</div> 	
				
				
						  <button type="submit" class="btn btn-primary" name="submit_zone">Save</button><div class="msg"></div>
						    
		            <?php echo form_close();?>
					<div class="table-responsive">
						<table class="table table-bordered  table-condensed setup_table">

					  		<thead>
					  			<tr>
					  				<th>Padlock Number</th>
					  				<th>Zone Number</th>
					  				<th>Returning Days</th>
					  				<th>WP Value</th>
					  				<th>Buffer(%)</th>
					  				<th>Irrigation Type</th>
					  				<th>Soil Type</th>
					  				<th>Fiel Capacity(%)</th>
					  				<th>Depth (/mm)</th>
					  				<th>MPDA (/mm)</th>
					  				<th>Option</th>
					  			</tr>
					  		</thead>
					  			<tbody>
					  			<?php
					  				$swd_values = array();
					  				$swd = array();
					  			 foreach ($things_zone_result as $key => $thing):

					  			 	if(isset($thing['properties']['swd']['value'])){
				  			 			$swd_values[] = $thing['properties']['swd']['value'];	
					  			 		$swd[] = array('thing_key'=>$thing['key'],'name'=>$thing['name'],'swd'=>@$thing['properties']['swd']['value']); 		
					  			 	}
					  			 	
					  			  ?>	
					  				<tr>
				  						<td class="padlock_num"><?php echo $thing['name'];?></td>
					  					<td class="zone"><?php echo @$thing['properties']['zone_num']['value'];?></td>
					  					<td class="returning_days"><?php echo @$thing['properties']['returning_days']['value'];?></td>
					  					<td class="wp_value"><?php echo @$thing['properties']['wp_value']['value'];?></td>
					  					<td class="buffer"><?php echo @$thing['properties']['buffer']['value'];?></td>
					  					<td class="irrigation_type">
					  							<input type="hidden" value="<?php echo (int)@$thing['properties']['irrigation_type']['value'];?>"/>
					  					<?php					  					 

					  					 switch ((int)@$thing['properties']['irrigation_type']['value']) {
					  					 	case 0:
					  					 			echo 'Water';
					  					 		break;
					  					 	case 1:
					  					 			echo 'Effluent';
					  					 		break;

					  					 }



					  					 ?></td>
					  					<td class="soil_type" alt="">
					  						<input type="hidden" value="<?php echo (int) @$thing['properties']['soil_type']['value']?>">
					  					<?php

											switch ((int)@$thing['properties']['soil_type']['value']) {
					  					 	case 1:
					  					 			echo 'Low Risk';
					  					 		break;
					  					 	case 2:
					  					 			echo 'High Risk';
					  					 		break;

					  					 }
					  					?>
					  						

					  					</td>
					  					<td class="field_capacity"><?php echo @$thing['properties']['field_capacity']['value'];?></td>
					  					<td class="depth"><?php echo @$thing['properties']['depth']['value'];?></td>
					  					<td class="mpda"><?php echo @$thing['properties']['mpda']['value'];?></td>
					  					<td><!-- <a href="#" class="glyphicon glyphicon-pencil"></a> --><button class="btn btn-primary edit_zone_info">Edit</button></td>
					  				</tr>
					  			<?php endforeach;?>	
					  				
					  			</tbody>
					  		</table>
					</div>
					  			

					  		<?php
								

								if( count($swd_values) > 0 ){
									

											$json = json_encode($swd);
							
								}

							?>
							<script type="text/javascript">
									
									var thing_result_object = <?php echo @$json;?>

							</script>
                
            </div>
            <div role="tabpanel" class="tab-pane fade" id="soil_sensor_factor">
            	<h3>Soil Moisture Sensor Factors</h3><br/>

            <div class="row">
            		<div class="col-md-6">
            			<form class="form-horizontal">

        					<fieldset>
							<!-- Select Basic -->
							<div class="form-group">
							  <label class="col-md-2 control-label" for="selectbasic">Select Thing</label>
							  <div class="col-md-10">
							  <select name="factor_sel" class="selectpicker form-control" data-live-search="true" data-style="btn-info">
							  <option data-tokens="<?php echo $thing['name']?>" value="" >Select Thing</option>
			            			<?php
			 							foreach ($things_zone_result as $key => $thing):
			            			?>
									  <option data-tokens="<?php echo $thing['name']?>" value="<?php echo $thing['name']?>"><?php echo $thing['name']?></option>
									<?php endforeach;?>					  
									</select>
							  </div>
							</div>

							</fieldset>
			      
	            		</form>
            		</div>
            </div>
				

				<div class="row">
					 <div class="col-md-12">
					 	            	  
            	  <div class="table-responsive ">
            	  		<?php echo  form_open('', ' id="soil_moisture_sensor_factor" style="padding:19px"');?>
                        	
                        	<div id="factor_form"></div>

                   		<?php echo form_close();?>	         
                      
                        </div>  

                   
					 </div>
				</div>

    	  	</div>
        </div>

		
      </p>

	 </div>