    
      <h3>Application Time</h3>
      <p >
		  <div class="at_msg"></div>

		 
			  <div id="application_time">

	

			  		<div class="row">
			  			<div class="col-xs-12 col-sm-12 col-md-6">
	  					 <h4>Paddock/Zone Settings</h4>
	  					 	<div class="table-responsive">
			  				  		<table class="table table-bordered  table-condensed ">
							  		<thead>
							  			<tr>
							  				<th>Paddock Number</th>
							  				<th>Zone Number</th>
							  				<th>Irrigation Type</th>
							  				<th>Application Time</th>
							  			</tr>
							  		</thead>
							  			<tbody>
							  			<?php  foreach($things_zone_result as $key => $thing):?>
							  				<tr>							  					
							  					<td><?php echo $thing['name'];?></td>
							  					<td><?php echo @$thing['properties']['zone_num']['value'];?></td>
							  					<td><?php
											  					 switch (@$thing['properties']['irrigation_type']['value']) {
											  					 	case 0:
											  					 			echo 'Water';
											  					 		break;
											  					 	case 1:
											  					 			echo 'Effluent';
											  					 		break;
											  					 	
											  					 	default:
											  					 			echo ' ';
											  					 		break;
											  					 }

												?></td>
							  					<td><?php echo @$thing['properties']['mpda']['value'];?></td>
							  				</tr>
							  		     <?php endforeach;?>
							  			</tbody>
							  		</table>
					  			</div>
			  			</div>

	  					<div class="col-xs-12 col-sm-12 col-md-6">
				  			<h4>POD Settings</h4>
				  			<div class="table-responsive">
				  				  		<table class="table table-bordered  table-condensed ">
								  		<thead>
								  			<tr>
								  				<th>POD Number</th>
								  				<th>SSID</th>
								  				<th>IP Address</th>
								  				<th>Password</th>
								  				<th>Application Rate(mm/hr)</th>
								  			</tr>
								  		</thead>
					  					<tbody>
								  				<?php  foreach ($things_zone_result as $key => $thing):


								  				?>
							  					<tr>
								  					<td><?php echo $thing['name'];?></td>
								  					<td><?php echo @$thing['attrs']['ssid']['value'];?></td>
								  					<td><?php echo @$thing['attrs']['adrs']['value'];?></td>
								  					<td><?php echo @$thing['attrs']['pswd']['value'];?></td>
			  										<td><?php echo @$thing['attrs']['ap_rate']['value']?></td>
								  				</tr>
						  						<?php endforeach;?>	
								  			</tbody>
								  		</table>
		
								  </div>		
			  			</div>
			  		
			  			
			  		</div>
			  </div>

      </p>
    