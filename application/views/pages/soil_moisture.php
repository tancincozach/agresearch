<div style="padding:20px;">
    <?php

				$monday = strtotime('last monday', strtotime('tomorrow'));
				$sunday = strtotime('+6 days', $monday);




		?>
  <div class="row">
    
    <div class="col-md-12">


    	<input class="form-control" type="hidden" name="selected_things_report" value=""/>
    	<input type="text" name="daterange" value="<?php echo  date('m/d/Y', $monday).' - '.date('m/d/Y', $sunday);?>" />
		<button name="bld_graph" class="btn btn-success"  onclick="setup.soil_moisture_graph();">Generate Graph</button>	
		<button name="btn_gen_report"    onclick="setup.generate_report();" class="btn btn-primary">Generate Report</button>

    </div>

  </div>

          <div class="row">
            		<div class="col-md-6">
            			<form class="form-horizontal">

        					<fieldset>
							<!-- Select Basic -->
							<div class="form-group">
							  <label class="col-md-2 control-label" for="selectbasic">Select Thing</label>
							  <div class="col-md-10">
							  <select name="thing_graph_drowpdown" class="selectpicker form-control" data-live-search="true" multiple  data-selected-text-format="count" data-style="btn-info"> 
							  <!-- <option data-tokens="thing_graph" value="" style="color:white !important">Select Thing</option> -->
			            			<?php
			 							foreach ($things_zone_result as $key => $thing):
			 								if(isset($thing['name'])):
			            			?>
									  <option data-tokens="<?php echo $thing['name']?>" value="<?php echo $thing['name']?>"><?php echo @$thing['name']?></option>
										<?php 
											endif;
									endforeach;?>					  
									</select>
							  </div>
							</div>

							</fieldset>
			      
	            		</form>
            		</div>
            </div>

	<div class="col-md-6 soil_moisture_table" style="min-height: 144px;height: 300px; overflow: auto">
			 <ul class="list-group thing_counter" >
			  	
			</ul>
		</div>    	

    <div style="clear:both"></div>
  <div  id="soil_moisture_line_graph" style="height: 500px; width: 100%;"></div>


</div>