<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Agresearch Admin</title>		

        <base href="<?php echo base_url(); ?>" />

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="assets/css/styles.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">        
        <link href="assets/plugins/map-icons-master/dist/css/map-icons.css" rel="stylesheet">                
        <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/icheck/flat/green.css" rel="stylesheet">

        <?php echo @$css_file; ?>
	</head>
	<body>


<?php $this->load->view('html-includes/header')?>
<?php $this->load->view('html-includes/side-bar' , @$data);?>

<script type="text/javascript">
    var agresearch_base_url = '<?php echo base_url(); ?>';

    var agresearch_global = {
        n:'<?php echo $this->security->get_csrf_token_name(); ?>',
        h:'<?php echo $this->security->get_csrf_hash(); ?>'        
    } 
</script>



<!-- Use any element to open the sidenav -->
<a href="#"  class="open_pane" ><i class="glyphicon glyphicon-triangle-right
" style="font-size:40px;" title="Open"></i></a>
<!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
<div id="main">

<div id="system-msg"></div>

    <div class="container-fluid">

          <div class="row">
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <div class="x_panel">
                       
                                <div class="x_content">


                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">

                                        <ul id="myTab" class="nav nav-tabs bar_tabs responsive" role="tablist">
                                            <li role="presentation" class="active"><a href="#setup"  class="main-tab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Setup</a>
                                            </li>
                                            <li role="presentation" class=""><a href="#soil_moisture" class="main-tab"  role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Soil Moisture</a>
                                            </li>
                                            <li role="presentation" class=""><a href="#application_time"  class="main-tab"  role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Application Time</a>

                                            <li role="presentation" class=""><a href="#results" role="tab" class="main-tab"  id="profile-tab2" data-toggle="tab" aria-expanded="false">Results</a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content responsive">
                                            <div role="tabpanel" class="tab-pane fade in active" id="setup">
                                                                                            
                                                <?php $this->load->view('pages/setup' ,  @$data);?>
                                    

                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="soil_moisture" >
                                            
                                                <?php $this->load->view('pages/soil_moisture' ,  @$data);?>                                                
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="application_time" >                                                                                                
                                                <?php $this->load->view('pages/application_time' ,  @$data);?>                                                
                                                
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="results">
                                                
                                                <?php $this->load->view('pages/results' ,  @$data);?>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                </div>

          </div>


    </div>


</div>
<!-- /Main -->


<?php $this->load->view('html-includes/footer',@$data);?>
    <!-- /.modal-dalog -->




<div class="modal" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Widget</h4>
            </div>
            <div class="modal-body">
                <p>Add a widget stuff here..</p>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>

<!-- /.modal -->
	<!-- script references -->



<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>


    <?php echo @$js_file; ?>
	</body>
</html>