<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Agresearch Login Page| </title>

    <!-- Bootstrap core CSS -->

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="assets/fonts/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/icheck/flat/green.css" rel="stylesheet">


    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
    
    <div class="">

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                <?php  echo form_open('', '  accept-charset="utf-8"   role="form" name="login-form" method="post"'); ?>
		
             
                        <h1>Login Form</h1>
                        <div>
                           	<input class="form-control" type="text" id="username" name="username" value="" placeholder="Username" >
                        </div>
                        <div>
                            	<input class="form-control" type="password" id="password" name="password" value="" placeholder="Password" />
                        </div>
                        <div class="text-left">

                            		<button class=" btn btn-default ">Sign In</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                     
                            <div class="clearfix"></div>
                            <br />
                            <div>

                                  <p>2017 Agresearch All Rights Reserved.s</p>
                            </div>
                        </div>
                   	<?php 

		echo form_close();

	?>

                </section>
                <!-- content -->
            </div>

        </div>
    </div>

</body>

</html>