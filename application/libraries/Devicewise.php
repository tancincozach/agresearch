<?php
class Devicewise{
  /**
   * Initialize the object.
   * @param array $options The initialization options.
   */

  protected $CI;

  public function __construct($options = array())
  {



     $this->CI =& get_instance();
     $this->CI->load->library('Rest' , $options);



  }


  /**
   * Convert a date string to TR50 format.
   * @param string $date The date string to reformat.
   * @return string The date formatted in the style that TR50 requires.
   */

  public static function date_format($date)
  {
    return date('c', strtotime($date));
  }


  /**
   * email.send : Sends an email to one or more email addresses from a registered user's email address in the M2M Service.
   * @param mixed $to A single email address: 'john.doe@example.com', or an array containing the recipients of the email: array('john.doe@example.com', 'jane.doe@example.com').
   * @param mixed $from A single email address: 'jane.doe@example.com', or an array containing an email address indexed by a name of the user to use as the sender of the email: array('Jane Doe' => 'jane.doe@example.com').
   * @param string $subject The subject of the email.
   * @param string $body The body of the email.
   * @return boolean If the command executes successfully.
   */

  public function email_send($to, $from, $subject, $body)
  {
    $params = array('subject' => strval($subject),
                    'body'    => strval($body));
    $params['to'] = (is_array($to)) ? $to : array($to);
    if (is_array($from)) {
      reset($from);
      $params['from'] = strval(current($from));
      $params['fromName'] = strval(key($from));
    }
    else {
      $params['from'] = strval($from);
    }
    if ($body != strip_tags($body)) {
      $params['html'] = true;
    }
    $result = $this->CI->rest->exec('email.send', $params);
    return ($result and $this->CI->rest->response['data']['success']);
  }


  /**
   * locale.get : This command is used to retrieve the current session's locale.
   * @return mixed Returns the array of the session's locale on success, or the failure code.
   */

  public function local_get()
  {
    $result = $this->CI->rest->exec('locale.get');
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * locale.list : This command is used to list supported locales.
   * @return mixed Returns the array of the supported locales on success, or the failure code.
   */

  public function locale_list()
  {
    $result = $this->CI->rest->exec('locale.list');
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * locale.set : This command is used to set the current session's locale.
   * @param string $locale The locale to set.
   * @return boolean If the command executes successfully.
   */

  public function locale_set($locale)
  {
    $params = array('locale' => strval($locale));
    $result = $this->CI->rest->exec('locale.set', $params);
    return ($result and $this->CI->rest->response['data']['success']);
  }


  /**
   * location.current : This command is used to obtain the last report location for a thing.
   * @param string $thingKey Identifies the thing associated with the location entry.
   * @return mixed Returns the array of the thing's current location on success, or the failure code.
   */

  public function location_current($thingKey)
  {
    $params = array('thingKey' => strval($thingKey));
    $result = $this->CI->rest->exec('location.current', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * location.decode : This command is used to decode a latitude/longitude pair into an address.
   * @param float $lat The latitude of the location.
   * @param float $lng The longitude of the location.
   * @return mixed Returns the array of the address of the latitude/longitude pair on success, or the failure code.
   */

  public function location_decode($lat, $lng)
  {
    $params = array('lat' => floatval($lat),
                    'lng' => floatval($lng));
    $result = $this->CI->rest->exec('location.decode', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * location.encode : This command is used to encode a textual location into a latitude/longitude pair.
   * @param string $location The textual location to be encoded.
   * @return mixed Returns the array of the latitude/longitude associated with the provided location on success, or the failure code.
   */

  public function location_encode($location)
  {
    $params = array('location' => strval($location));
    $result = $this->CI->rest->exec('location.encode', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * location.publish : This command is used to publish the location of a thing.
   * @param string $thingKey The unique identifier of the thing to which the location data is to be associated.
   * @param float $lat The latitude for this location publish.
   * @param float $lng The longitude for this location publish.
   * @param string $ts The timestamp for which the location data is being published.
   * @param int $heading The direction for this location publish (in degrees where 0 is North, 90 is East, 180 is South, and 270 is West).
   * @param int $altitude The altitude for this location publish.
   * @param int $speed The speed for this location publish.
   * @param int $fixAcc The accuracy in meters of the coordinates being published.
   * @param string $fixType A string describing the location fixation type. Typically "gps", "gnss", "manual", or "m2m-locate".
   * @param string $corrId A correlation ID that can be used when querying to find related data objects.
   * @return boolean If the command executes successfully.
   */

  public function location_publish($thingKey, $lat, $lng, $ts = false, $heading = false, $altitude = false, $speed = false, $fixAcc = false, $fixType = false, $corrId = false)
  {
    $params = array('thingKey' => strval($thingKey),
                    'lat'      => floatval($lat),
                    'lng'      => floatval($lng));
    if ($ts !== false) {
      $params['ts'] = self::date_format($ts);
    }
    if ($heading !== false) {
      $params['heading'] = intval($heading);
    }
    if ($altitude !== false) {
      $params['altitude'] = intval($altitude);
    }
    if ($speed !== false) {
      $params['speed'] = intval($speed);
    }
    if ($fixAcc !== false) {
      $params['fixAcc'] = intval($fixAcc);
    }
    if ($fixType !== false) {
      $params['fixType'] = strval($fixType);
    }
    if ($corrId !== false) {
      $params['corrId'] = strval($corrId);
    }
    $result = $this->CI->rest->exec('location.publish', $params);
    return ($result and $this->CI->rest->response['data']['success']);
  }


  /**
   * location.speedlimit : This command is used to find the speed limit at a specified location.
   * @param float $lat The latitude being requested.
   * @param float $lng The longitude being requested.
   * @return mixed Returns the array of the speed limit associated with the provided location on success, or the failure code.
   */

  public function location_speed_limit($lat, $lng)
  {
    $params = array('lat' => floatval($lat),
                    'lng' => floatval($lng));
    $result = $this->CI->rest->exec('location.speedlimit', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * location.weather : This command is used to return the weather information at a specified location.
   * @param float $lat The latitude being requested.
   * @param float $lng The longitude being requested.
   * @return mixed Returns the array of the weather information associated with the provided location on success, or the failure code.
   */

  public function location_weather($lat, $lng)
  {
    $params = array('lat' => floatval($lat),
                    'lng' => floatval($lng));
    $result = $this->CI->rest->exec('location.weather', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * method.exec : This command is used to execute a method of a thing.
   * @param string $thingKey Identifies the thing for which the method is executed.
   * @param string $method The method to execute.
   * @param bool $singleton Set to true if this call is to be run as a singleton.
   * @param int $ackTimeout Acknowlege timeout duration in seconds. Default is 30.
   * @param array $parameters Notification variables or method parameters to be passed to the method.
   * @return bool If the command executes successfully.
   */

  public function method_exec($thingKey, $method, $singleton = false, $ackTimeout = false, $parameters = false)
  {
    $params = array('thingKey' => $thingKey,
                    'method'   => $method);
    if ($singleton !== false) {
      $params['singleton'] = (bool)$singleton;
    }
    if ($ackTimeout !== false) {
      $params['ackTimeout'] = intval($ackTimeout);
    }
    if ($parameters !== false and is_array($parameters)) {
      $params['params'] = $parameters;
    }
    $result = $this->CI->rest->exec('method.exec', $params);
    return ($result and $this->CI->rest->response['data']['success']);
  }


  /**
   * org.find : This command is used to find and return an organization.
   * @param string $key The key identifying the organization.
   * @return mixed Returns the array of the organization information on success, or the failure code.
   */

  public function org_find($key)
  {
    $params = array('key' => strval($key));
    $result = $this->CI->rest->exec('org.find', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * org.list : This command is used to return a list of organizations.
   * @param int $offset The starting list offset, used for pagination, defaults to 0 if not specified.
   * @param int $limit Limits the number of results returned. Defaults to the maximum configured size.
   * @param bool $canHaveSubOrgs Whether to only list organizations that are capable of having child organizations.
   * @return mixed Returns an array of organizations on success, or the failure code.
   */

  public function org_list($offset = false, $limit = false, $canHaveSubOrgs = false)
  {
    $params = array();
    if ($offset !== false) {
      $params['offset'] = intval($offset);
    }
    if ($limit !== false) {
      $params['limit'] = intval($limit);
    }
    if ($canHaveSubOrgs !== false) {
      $params['canHaveSubOrgs'] = (bool)$canHaveSubOrgs;
    }
    $result = $this->CI->rest->exec('org.list', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * property.aggregate : This command is used to obtain historical property data aggregated over a specified time period for a thing.
   * @param string $thingKey Identifies the thing to which the property data is to be associated.
   * @param string $key The key for the property that you wish to aggregate.
   * @param string $calc The calculation to use for the aggregation: avg, sum, max, min, count, etc.
   * @param string $series The series to be used when grouping property values to aggregate: 'hour' or 'day'.
   * @param string $start Timestamp for the start of the specified time window.
   * @param string $end Timestamp for the end of the specified time window.
   * @param bool $split Set to true if you want the timestamp and value fields to be split into two arrays.
   * @return mixed Returns the array of property aggregation on success, or the failure code.
   */

  public function property_aggregate($thingKey, $key, $calc, $series, $start, $end, $split = false)
  {
    $params = array('thingKey' => strval($thingKey),
                    'key'      => strval($key),
                    'calc'     => strval($calc),
                    'series'   => strval($series),
                    'start'    => self::date_format($start),
                    'end'      => self::date_format($end),
                    'split'    => (bool)$split);
    $result = $this->CI->rest->exec('property.aggregate', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * property.current : This command is used to retrieve the current value of a property.
   * @param string $thingKey Identifies the thing to which the property data is associated.
   * @param string $key The key for the property that you wish to retrieve.
   * @return mixed Returns the array of current property on success, or the failure code.
   */

  public function property_current($thingKey, $key)
  {
    $params = array('thingKey' => strval($thingKey),
                    'key'      => strval($key));
    $result = $this->CI->rest->exec('property.current', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * property.publish : The property.publish command is used to publish property data (typically sensor data) for a thing.
   * @param string $thingKey Identifies the thing to which the property data is to be associated.
   * @param string $key The key for the property that you wish to publish.
   * @param float $value The value to publish.
   * @param string $ts The timestamp in which the value was recorded.
   * @param string $corrId A correlation ID that can be used when querying to find related data objects.
   * @return boolean If the command executes successfully.
   */

  public function property_publish($thingKey, $key, $value, $ts = false, $corrId = false)
  {
    $params = array('thingKey' => strval($thingKey),
                    'key'      => strval($key),
                    'value'    => $value);


    if ($ts !== false) {
      $params['ts'] = self::date_format($ts);
    }
    if ($corrId !== false) {
      $params['corrId'] = strval($corrId);
    }


    $result = $this->CI->rest->exec('property.publish', $params);
   


    return ($result and $this->CI->rest->response['data']['success']);
  }



 /**
   * property.publish : The property.publish command is used to publish property data (typically sensor data) for a thing.
   * @param string $thingKey Identifies the thing to which the property data is to be associated.
   * @param string $key The key for the property that you wish to publish.
   * @param float $value The value to publish.
   * @param string $ts The timestamp in which the value was recorded.
   * @param string $corrId A correlation ID that can be used when querying to find related data objects.
   * @return boolean If the command executes successfully.
   */

  public function property_batch($thingKey, $data = array() ,$ts = false, $corrId = false)
  {
    $params = array('thingKey' => strval($thingKey),
                    'data'      => $data);
    if ($ts !== false) {
      $params['ts'] = self::date_format($ts);
    }
    if ($corrId !== false) {
      $params['corrId'] = strval($corrId);
    }



    $result = $this->CI->rest->exec('property.batch', $params);


    return ($result and $this->CI->rest->response['data']['success']);
  }



  /**
   * property_history : This command is used to check an alarm and returns it.
   * @param string $key Identifies the thing.
    * @param string $alarm Identifies the alarm key.
   * @return mixed Returns the array of the selected thing on success, or the failure code.
   */

  public function property_history($thing_key , $prop_key,$records){

    $params = array('key' => strval($prop_key) , 'thingKey'=>  strval($thing_key) ,'records'=>$records);

    

    $result = $this->CI->rest->exec('property.history', $params);   

    return ($result and isset( $this->CI->rest->response['data']['success'])) ? $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }



  public function session_find($sess_id)
  {
     $params = array(
      'id'=>@strval($sess_id)
      );

    $result = $this->CI->rest->exec('session.find', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }

  /**
   * session.info : This command is used to obtain information about the current session.
   * @return mixed Returns the array of current session on success, or the failure code.
   */

  public function session_info()
  {

    $result = $this->CI->rest->exec('session.info');

    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * session.org.list : This command is used to obtain a list of organizations available to the current session.
   * @param boolean $includeRoles Indicate that the available roles are to be returned with the response.
   * @return mixed Returns the array of organizations (and roles) on success, or the failure code.
   */

  public function session_org_list($includeRoles = false)
  {
    $params = array();
    if ($includeRoles !== false) {
      $params = array('includeRoles' => (bool)$includeRoles);
    }
    $result = $this->CI->rest->exec('session.org.list', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * session.org.switch : This command is used to switch a session between organizations.
   * @param string $key The organization key.
   * @return boolean If the command executes successfully.
   */

  public function session_org_switch($key)
  {

    $params = array('key' => strval($key));
    $result = $this->CI->rest->exec('session.org.switch', $params);

    return ($result and $this->CI->rest->response['data']['success']);
  }


  /**
   * thing_def.find : This command is used to find an existing thing definition.
   * @param string $key The key of the thing definition to find.
   * @return mixed Returns the array of the selected thing definition on success, or the failure code.
   */

  public function thing_def_find($key)
  {
    $params = array('key' => strval($key));
    $result = $this->CI->rest->exec('thing_def.find', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }




  /**
   * trigger.action.list : The trigger.action.list command is used to list all trigger action types with their inputs and outputs
   * @param string $key The key of the thing definition to find.
   * @return mixed Returns the array of the selected thing definition on success, or the failure code.
   */

  public function create_radial_geoference($name , $thingKey,$x)
  {

    $coordinates = array(floatval($x[0]),floatval($x[1]));


    $params = array('name' => strval($name),
                    'key'      => strval($thingKey),
                    'points'    => array($coordinates),
                    'radius'=>0.5);

    $result = $this->CI->rest->exec('geofence.create', $params);       



      return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }
  
 /**
   * geofence.list : The geofence.list command is used to list all geofence
   * @param string $key The key of the thing definition to find.
   * @return mixed Returns the array of the selected thing definition on success, or the failure code.
   */

  public function geofence_list()
  {
    $result = $this->CI->rest->exec('geofence.list');
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }

  /**
   * geofence.delete : The geofence.delete command is used to delete existing geofence 
   * @param string $key The key of the thing definition to find.
   * @return mixed Returns the array of the selected thing definition on success, or the failure code.
   */

  public function delete_geofence($geofence_key)
  {

    $params = array(
                      'key' => strval($geofence_key)
                    );

    $result = $this->CI->rest->exec('geofence.delete', $params);   
   
      return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  public function trigger_create($parameters)
  {
    $params = array(
      'name'=>@strval($parameters['name']),
      'eventType'=>@strval($parameters['event_type']),
      'key' => strval($parameters['key']),
      'event'=>@$parameters['event'],
      'actions'=>array(@$parameters['actions'])
      );

    $result = $this->CI->rest->exec('trigger.create', $params);

    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


    public function trigger_update($parameters)
   {
      $params = array(
      'name'=>@strval($parameters['name']),
      'id'=>@strval($parameters['id']),
      'started' => @$parameters['start'],
      'key'=>strval($parameters['key'])
      );


    $result = $this->CI->rest->exec('trigger.update', $params);
        
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }

 /**
   * trigger.action.list : The trigger.action.list command is used to list all trigger action types with their inputs and outputs
   * @param string $key The key of the thing definition to find.
   * @return mixed Returns the array of the selected thing definition on success, or the failure code.
   */

  public function trigger_action_list($key)
  {
    $params = array('key' => strval($key));
    $result = $this->CI->rest->exec('trigger.action.list', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }
 /**
   * trigger.action.list : The trigger.action.list command is used to list all trigger action types with their inputs and outputs
   * @param string $key The key of the thing definition to find.
   * @return mixed Returns the array of the selected thing definition on success, or the failure code.
   */

  public function trigger_list($key)
  {
    $params = array('key' => strval($key));
    $result = $this->CI->rest->exec('trigger.list', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }

  public function trigger_delete($parameter=array())
  {
    $params = array(
            'key' => strval($parameter['key']),
            'id' => strval($parameter['id'])
      );

    $result = $this->CI->rest->exec('trigger.delete', $params);  



    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }
  /**
   * thing_def.list : Acquire the list of Thing Definitions.
   * @param int $offset The starting list offset, used for pagination, defaults to 0 if not specified.
   * @param int $limit Limits the number of results returned. Defaults to the maximum configured size.
   * @return mixed Returns the array of thing definitions on success, or the failure code.
   */

  public function thing_def_list($offset = false, $limit = false)
  {
    $params = array();
    if ($offset !== false) {
      $params['offset'] = intval($offset);
    }
    if ($limit !== false) {
      $params['limit'] = intval($limit);
    }
    $result = $this->CI->rest->exec('thing_def.list', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }






  /**
   * thing.find : This command is used to find and return a thing.
   * @param string $key Identifies the thing.
   * @return mixed Returns the array of the selected thing on success, or the failure code.
   */

  public function thing_find($key)
  {
    $params = array('key' => strval($key));


    $result = $this->CI->rest->exec('thing.find', $params);   
/*
      echo '<pre>';
        print_r($params);
      echo '<pre>';
      echo '<hr>';
      echo '<pre>';
        print_r($result);
      echo '<pre>';
      echo '<hr>';
    exit();*/

    return ($result and isset( $this->CI->rest->response['data']['success'])) ? @$this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }






 /**
   * thing.bind : This command is used to bind nd and return a thing.
   * @param string $key Identifies the thing.
   * @return mixed Returns the array of the selected thing on success, or the failure code.
   */

  public function thing_bind($auth,$params)
  {
    $params = array(
      'key'=>@strval($params['key']),
      'autoDefKey'=>@strval($params['defkey'])
      //,'bindTo'=>@strval($params['bindTo'])        
      );



     $this->CI->load->library('Rest' , $auth);

    $result = $this->CI->rest->exec('thing.bind', $params);   
    

    return ($result and isset( $this->CI->rest->response['data']['success'])) ? $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }



  /**
   * thing.list : This command is used to find and return a list of things.
   * @param int $offset The starting list offset, used for pagination. Defaults to 0 if not specified.
   * @param int $limit Limits the number of results returned. Defaults to the maximum configured size.
   * @param array $show An array of field names of the data columns to return.
   * @param string $sort A string indicated the direction ("+" for ascending, "-" for descending) and column to sort the results by. To sort by the key descending, use "-key". Defaults to "+name".
   * @param array $tags If specified, the command will only return things that have all tags in this parameter.
   * @param array $keys If specified, the command will only return things that have the keys specified in this parameter.
   * @return mixed Returns the array of things on success, or the failure code.
   */

  public function thing_list($offset = false, $limit = false, $show = false, $sort = false, $tags = false, $keys = false,$has_loc=false)
  {
    $params = array();
    if ($offset !== false) {
      $params['offset'] = intval($offset);
    }
    if ($limit !== false) {
      $params['limit'] = intval($limit);
    }
    if ($show !== false and is_array($show)) {
      $params['show'] = $show;
    }
    if ($sort !== false) {
      $params['sort'] = strval($sort);
    }
    if ($tags !== false and is_array($tags)) {
      $params['tags'] = $tags;
    }
    if ($keys !== false and is_array($keys)) {
      $params['keys'] = $keys;
    }
    if($has_loc !== false) {
      $params['hasLoc'] = $has_loc;
    }

    $result = $this->CI->rest->exec('thing.list', $params);



    return ($result and @ $this->CI->rest->response['data']['success']) ? @ $this->CI->rest->response['data']['params']['result'] : @ $this->CI->rest->response['data']['success'];
  }


  /**
   * twilio.sms.send : Send an SMS message to a phone number.
   * @param mixed $to The recipient's phone number, a comma separated string of phone numbers, or an array of phone numbers.
   * @param string $body The body of the SMS message.
   * @param mixed $from The sender's phone number.
   * @return boolean If the command executes successfully.
   */

  public function twilio_sms_send($to, $body, $from = false)
  {
    $params = array('body' => strval($body));
    $params['to'] = (is_array($to)) ? implode(',', $to) : strval($to);
    if ($from !== false and strlen($from) > 0) {
      $params['from'] = strval($from);
    }
    $result = $this->CI->rest->exec('twilio.sms.send', $params);
    return ($result and $this->CI->rest->response['data']['success']);
  }


  /**
   * user.find : This command is used to retrieve a user by email address.
   * @param string $emailAddress The email address of the user.
   * @return mixed Returns the array of the user on success, or the failure code.
   */

  public function user_find($emailAddress)
  {
    $params = array('emailAddress' => strval($emailAddress));
    $result = $this->CI->rest->exec('user.find', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * user.list : This command is used to return a list of users.
   * @param int $offset The starting list offset, used for pagination, defaults to 0 if not specified.
   * @param int $limit Limits the number of results returned. Defaults to the maximum configured size.
   * @return mixed Returns an array of users on success, or the failure code.
   */

  public function user_list($offset = false, $limit = false)
  {
    $params = array();
    if ($offset !== false) {
      $params['offset'] = intval($offset);
    }
    if ($limit !== false) {
      $params['limit'] = intval($limit);
    }
    $result = $this->CI->rest->exec('user.list', $params);
    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * alarm.history : This command is used to check an alarm and returns it.
   * @param string $key Identifies the thing.
    * @param string $alarm Identifies the alarm key.
   * @return mixed Returns the array of the selected thing on success, or the failure code.
   */

  public function alarm_history($thing_key , $alarm){

    $params = array('key' => strval($alarm) , 'thingKey'=>  strval($thing_key) ,'records'=>1);

    

    $result = $this->CI->rest->exec('alarm.history', $params);   

    return ($result and isset( $this->CI->rest->response['data']['success'])) ? $this->CI->rest->response['data']['params'] : $this->CI->rest->response['data']['success'];
  }


    /**
   * log.list : Acquire the list of log .
   * @param array $params 
   * @param int $limit
   * @param int $offset
   * @return mixed Returns the array of thing definitions on success, or the failure code.
   */

  public function log_list($thing_key='',$cor_id='',$params = array(),$offset = false, $limit = false)
  {

     $params = array();

    if ($offset !== false) {
      $params['offset'] = intval($offset);
    }

    if ($limit !== false) {
      $params['limit'] = intval($limit);
    }

    if($thing_key!=''){
      $params['thingKey'] = $thing_key;
    }

    if($cor_id!=''){
      $params['corrId'] = strval($cor_id);
    }


    $result = $this->CI->rest->exec('log.list', $params);


    return ($result and $this->CI->rest->response['data']['success']) ?
      $this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }

    /**
   * log.list : Publish log .
   * @param array $params 
   * @param int $limit
   * @param int $offset
   * @return mixed Returns the array of thing definitions on success, or the failure code.
   */

  public function publish_log($thing_key,$params)
  {    

    
    if($thing_key!=''){
      $params['thingKey']  = $thing_key;
    }

    if(count($params) > 0){
     $params['msg']  = json_encode($params); 
    }

    $params['corrId'] = strval($thing_key.'-publish');


    $result = $this->CI->rest->exec('log.publish', $params);





    return ($result and $this->CI->rest->response['data']['success']) ?
      @$this->CI->rest->response['data']['params']['result'] : $this->CI->rest->response['data']['success'];
  }


  /**
   * attribute.publish : The attribute.publish command is used to publish property data (typically sensor data) for a thing.
   * @param string $thingKey Identifies the thing to which the property data is to be associated.
   * @param string $key The key for the property that you wish to publish.
   * @param float $value The value to publish.
   * @param string $ts The timestamp in which the value was recorded.
   * @param string $corrId A correlation ID that can be used when querying to find related data objects.
   * @return boolean If the command executes successfully.
   */

  public function attr_publish($thingKey, $key, $value, $ts = false, $corrId = false){
    $params = array('thingKey' => strval($thingKey),
                    'key'      => strval($key),
                    'value'    => $value);


    if ($ts !== false) {
      $params['ts'] = self::date_format($ts);
    }
    if ($corrId !== false) {
      $params['corrId'] = strval($corrId);
    }

    $result = $this->CI->rest->exec('attribute.publish', $params);
   
    return ($result and $this->CI->rest->response['data']['success']);
  }
}
?>