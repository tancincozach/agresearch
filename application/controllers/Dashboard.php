<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {


	 public  $result_array = array() ;



	public $session_data = array();

	public $auth_settings = array();

	public function __construct()
	{
		parent::__construct();		

		$this->session_data = $this->session->userdata;

		$this->load->model('Agresearchmodel','agresearch');

		$this->auth_settings['sessionId'] = @$this->session->userdata('devicewise_response')['sessionId'];
		$this->auth_settings['_endPoint'] = DEVICE_WISE_API;
		$this->auth_settings['userName'] = $this->session->userdata('uname');
		$this->auth_settings['userPass'] = $this->session->userdata('pass');

		$this->result_array  = $this->agresearch->segregate_zone_things($this->auth_settings);

    	$this->view_data['data'] = $this->result_array;

/*    echo "<pre>";

   	print_r($this->result_array ['things_zone_result']);

   	echo "<pre/>";

   	exit();
*/
	}
	

	public function index()
	{

		$this->setup();

		
	}

	
	public function setup(){




			$this->view_data['menu_active'] = 'setup';

			$this->view_data['view_file'] = 'pages/setup';


			/*$this->view_data['js_file'] = '<script src="assets/plugins/highcharts/highcharts.js"></script>'.PHP_EOL;

			$this->view_data['js_file'] .= '<script src="assets/plugins/highcharts/highcharts-more.js"></script>'.PHP_EOL;

			$this->view_data['js_file'] .= '<script src="assets/plugins/highcharts/modules/exporting.js"></script>'.PHP_EOL;*/

			//$this->view_data['js_file'] .= '<script src="assets/plugins/canvasjs-1.9.10-stable/canvasjs.min.js"></script>'.PHP_EOL;
			
			
		/*	$this->view_data['css_file'] ='<link rel="stylesheet" href="http://www.amcharts.com/lib/style.css" type="text/css">'.PHP_EOL;
			$this->view_data['js_file'] ='<script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>'.PHP_EOL;
			$this->view_data['js_file'] .='<script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>'.PHP_EOL;*/

			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/plugins/canvasjs-1.9.10-stable/canvasjs.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/plugins/daterangepicker/js/moment.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/plugins/daterangepicker/js/daterangepicker.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/responsive-tabs.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/plugins/bootstrap-select-1.12.4/js/bootstrap-select.min.js"></script>'.PHP_EOL;
			$this->view_data['js_file'] .= '<script type="text/javascript" src="assets/js/setup.js"></script>'.PHP_EOL;

			$this->view_data['css_file'] = '<link rel="stylesheet" type="text/css" href="assets/plugins/daterangepicker/css/daterangepicker.css" />'.PHP_EOL;
			$this->view_data['css_file'] .= '<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-select-1.12.4/css/bootstrap-select.min.css" />'.PHP_EOL;
			


			

			$this->load->view('index', $this->view_data);
	}

	public function soil_moisture(){
			$this->view_data['menu_active'] = 'soil_moisture';

			$this->view_data['view_file'] = 'pages/soil_moisture';



			$this->load->view('index', $this->view_data);
	}
	public function application_time(){
			$this->view_data['menu_active'] = 'application_time';

			$this->view_data['view_file'] = 'pages/application_time';

			$this->load->view('index', $this->view_data);
	}
	public function results(){
			$this->view_data['menu_active'] = 'results';

			$this->view_data['view_file'] = 'pages/results';

			$this->load->view('index', $this->view_data);
	}

	function generate_report(){


	}
}
