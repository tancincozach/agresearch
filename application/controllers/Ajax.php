<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	 		

	public $result_array = array();

	protected $auth_settings =array();
	
	public function __construct(){

		parent::__construct();


	/*
		WEOLCAN CREDENTIALS
		Username: weolcan@iotzone.co
		Password: S1MenormalX72.

	;*/

		$sess = $this->session->userdata;

	 	if(array_key_exists('sessionId', $sess['devicewise_response'])){

				$this->load->model('Agresearchmodel','agresearch');

 				$this->auth_settings['sessionId'] = $sess['devicewise_response']['sessionId'];	
				$this->auth_settings['_endPoint'] = DEVICE_WISE_API;
							
				$this->result_array  = $this->agresearch->segregate_zone_things($this->auth_settings);




	 	} 


		$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
		strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
		if(!$isAjax) {

		    header('HTTP/1.0 404 Not Found');
		    echo "<h1>404 Not Found</h1>";
		    echo "The page that you have requested could not be found.";		  
		  
		  	exit();
		}	



		if( !$this->session->userdata('logged_in')){
				
				$cookie = get_cookie('sesshpr');
				$cookie = base64_decode($cookie);
				$cookie = json_decode($cookie);

				if(isset($cookie->logged_in)){


					$sess['logged_in'] 	= 1; 
					$sess['devicewise_response'] 	=  $cookie['devicewise_response']['auth']['params'];
					$sess['uname'] 	= $cookie['uname'];
					$sess['pass'] 	= $cookie['pass'];

					
			
					$this->session->set_userdata($sess);
				}

		}
	}
	

	public function get_things(){
		

			if( count($this->result_array) > 0 ){

				if(count($this->result_array) == 1){

						$json = json_encode($this->result_array[0]);
				}else{
						$json = json_encode($this->result_array);
				}

				echo $json;
			}
		
	}

	public function implement_configuration(){

		try {


				$this->load->helper('file');

				$post = $this->input->post();

				$return = array('error'=>0);

				$settings  =  json_decode( $this->load->view('html-includes/config.json','',true),TRUE);
			
				foreach ($post as $key => $value) {
							
							$settings[$key] = $value;
				}

				if(count($settings) == 0 ) throw new Exception("Empty configuration");

				$json_output = json_encode($settings);

				if ( ! write_file(APPPATH.'views/html-includes/config.json', $json_output)) throw new Exception("Unable to write config");
				
				echo json_encode(array_merge(array('msg'=>'Configuration succesfully implemented.'),array('config'=>$settings)));				
			
		} catch (Exception $e) {

				echo json_encode(array('error'=>$e->getMessage()));	
			
		}


		

	}


	public function get_config(){

				$this->load->helper('file');

				$post = $this->input->post();

				$return = array('error'=>0);

				$json_output  = $this->load->view('html-includes/config.json','',true);
						

				echo $json_output;

	}

	public function submit_zone_settings(){
		

			$post = $this->input->post();
			$posted_array = array();
			$posted_array['buffer'] =$this->input->post('buffer');
			$posted_array['field_capacity'] = $this->input->post('field_capacity');
			$posted_array['pod_num'] =  $this->input->post('pod_num');			
			$posted_array['wp_value'] =  $this->input->post('wp_value');
			$posted_array['zone_num'] = $this->input->post('zone_num');
			$posted_array['soil_type'] = $this->input->post('soil_type');
			$posted_array['returning_days'] = $this->input->post('returning_days');
			$posted_array['irrigation_type'] = (isset($_POST['irrigation_type']) ? 1:0);

			$this->agresearch->publish_properties($this->auth_settings,$posted_array);


			unset($posted_array);

			return true;
			
	}

	public function submit_factor_settings(){

		   $post = $this->input->post();

			$thing_name = $post['thing_key'];

			unset($post['thing_key']);


			$this->agresearch->publish_factor($this->auth_settings,$thing_name,$post);

			unset($posted_array);

			return true;
	
			
	}/*
	public function submit_factor_settings(){


			$post = $this->input->post();
		

			$this->agresearch->publish_factor($this->auth_settings,	$this->result_array['things_zone_result'] ,$post);

			unset($posted_array);

	
			
	}*/

	public function submit_pod(){

			$post = $this->input->post();

			$thing_name = $post['thing_key'];

			unset($post['thing_key']);


			$this->agresearch->publish_pod($this->auth_settings,$thing_name,$post);

			unset($posted_array);

	}

	public function applicaction_time(){
			if( count($this->result_array) > 0 ){
			?>

			  		<div class="row">
			  			<div class="col-md-6">
	  					 <h4>Paddock/Zone Settings</h4>
			  				  		<table class="table  table-bordered ">
							  		<thead>
							  			<tr>
							  				<th>Paddock Number</th>
							  				<th>Zone Number</th>
							  				<th>Irrigation Type</th>
							  				<th>Application Time</th>
							  			</tr>
							  		</thead>
							  			<tbody>
							  			<?php  foreach($things_zone_result as $key => $thing):?>
							  				<tr>							  					
							  					<td><?php echo $thing['name'];?></td>
							  					<td><?php echo @$thing['properties']['zone_num']['value'];?></td>
							  					<td><?php
											  					 switch (@$thing['properties']['irrigation_type']['value']) {
											  					 	case 0:
											  					 			echo 'Water';
											  					 		break;
											  					 	case 1:
											  					 			echo 'Effluent';
											  					 		break;
											  					 	
											  					 	default:
											  					 			echo ' ';
											  					 		break;
											  					 }

												?></td>
							  					<td><?php echo @$thing['properties']['mpda']['value'];?></td>
							  				</tr>
							  		     <?php endforeach;?>
							  			</tbody>
							  		</table>
			  			</div>

	  					<div class="col-md-6">
				  			<h4>POD Settings</h4>
				  				  		<table class="table  table-bordered ">
								  		<thead>
								  			<tr>
								  				<th>POD Number</th>
								  				<th>SSID</th>
								  				<th>IP Address</th>
								  				<th>Password</th>
								  				<th>Application Rate(mm/hr)</th>
								  			</tr>
								  		</thead>
					  					<tbody>
								  				<?php  foreach ($things_zone_result as $key => $thing):


								  				?>
							  					<tr>
								  					<td><?php echo $thing['name'];?></td>
								  					<td><?php echo @$thing['attrs']['ssid']['value'];?></td>
								  					<td><?php echo @$thing['attrs']['adrs']['value'];?></td>
								  					<td><?php echo @$thing['attrs']['pswd']['value'];?></td>
			  										<td><?php echo @$thing['attrs']['ap_rate']['value']?></td>
								  				</tr>
						  						<?php endforeach;?>	
								  			</tbody>
								  		</table>
		

			  			</div>
			  		
			  			
			  		</div>

			<?php
		}

	}
		public function results(){
			if( count($this->result_array) > 0 ){
			?>
<div class="table-responsive">
											  		<table class="table  table-bordered ">
											  		<thead>
											  			<tr>
											  				<th>Padock #</th>
											  				<th>Zone #</th>
											  				<th>Date</th>
											  				<th>Irrigation Type</th>
											  				<th>SWD</th>
											  				<th>Actual Dad</th>
											  				<th>AT</th>
											  				<th>Remaining Time</th>
											  				<th>Start Time</th>
											  				<th>Stop Time</th>
											  				<th>Start Time</th>
											  				<th>Stop Time</th>
											  				<th>Start Time</th>
											  				<th>Stop Time</th>
											  			</tr>
											  		</thead>
											  			<tbody>
											  			<?php  foreach($this->result_array['things_zone_result']  as $key => $thing):?>
											  			<tr>
											  				<td><?php echo $thing['name'];?></td>
											  				<td><?php echo @$thing['properties']['zone_num']['value'];?></td>
											  				<td>N/A</td>
											  				<td ><?php


											  					 switch (@$thing['properties']['irrigation_type']['value']) {
											  					 	case 0:
											  					 			echo 'Water';
											  					 		break;
											  					 	case 1:
											  					 			echo 'Effluent';
											  					 		break;
											  					 	
											  					 	default:
											  					 			echo ' ';
											  					 		break;
											  					 }




											  					 ?></td>
											  				<td><?php echo  @$thing['properties']['swd']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['actual_dad']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['at']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['remaining_time']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['start_time_1']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['stop_time_1']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['start_time_2']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['stop_time_2']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['start_time_3']['value'];?></td>
											  				<td><?php echo  @$thing['properties']['stop_time_3']['value'];?></td>
											  			</tr>
											  			<?php endforeach;?>	
											  			</tbody>
											  			</table>
											  			</div>
	      </div>

			<?php
		}
	}

	public function soil_moisture(){
		if( count($this->result_array) > 0 ){
			?>

				<table class="table table-bordered table-condensed ">
				<tr class="info">
					<th><input type="checkbox" class="check_all_things"/></th>
					<th>Thing Name</th>
				</tr>
				<tbody>
				<?php 	 foreach ($this->result_array['things_zone_result']  as $key => $thing): ?>
					<tr>
						<td><input type="checkbox" class="soil_moisture_sel_box"  value="<?php echo $thing['name'];?>"></td>
						<td><?php echo $thing['name'];?></td>
					</tr>
				<?php endforeach;?>

				</tbody>
			</table>
			<button class="btn btn-primary bld_graph" disabled='disabled' onclick="setup.soil_moisture_graph();">Build Graph</button>

			<?php
		}	
	}

	public function json_graph(){

		$post  = $this->input->post();

		if(isset($post['sel_box'])){

			$exp_things  = explode(',',$post['sel_box']);

			$selected_things = array();



			$monday = strtotime('last monday', strtotime('tomorrow'));

			$sunday = strtotime('+6 days', $monday);

			$date_range_arr = isset($post['daterange']) ? explode('-',$post['daterange']):array($monday,$sunday);


			foreach ($this->result_array['things_zone_result'] as $key => $thing) {




				if(in_array( $thing['name'], $exp_things)){					

					$swd_history = $this->agresearch->prop_history($this->auth_settings,$thing['key'],'SWC');

					$swc_history = array();

					if(count($swd_history) > 0){

							foreach ($swd_history as $key => $value) {

		 						$date_ts = 	date('Y-m-d',strtotime($value['ts']));

		 						if (($date_ts >= date('Y-m-d',strtotime(trim($date_range_arr[0])))) && ($date_ts <= date('Y-m-d',strtotime(trim($date_range_arr[1]))))){									
									$swc_history[$key] = array('new_date'=>$value['ts'],'ts'=>$value['ts'],'value'=>$value['value']);
								}					  
						
							}
						$thing['properties']['swc_history'] = $swc_history;	
					
					}	
					if(isset($thing['properties']['swc_history']) && count($thing['properties']['swc_history']) > 0){
						$selected_things[] =  $thing;
					}
	
				}
			
			}	

			$json_output = json_encode($selected_things);


		}

		echo $json_output;

				

	}

	public function generate_report(){

		
			$this->load->helper('file');

			$this->load->library('Excel'); 

			$post = $this->input->post();

			$monday = strtotime('last monday', strtotime('tomorrow'));

			$sunday = strtotime('+6 days', $monday);

			$date_range_arr = isset($post['daterange']) ? explode('-',$post['daterange']):array($monday,$sunday);

			$selected_things = array();

			$data = array();


			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Soil Moisture'.rand(date('mdYHis'), 5));
			$this->excel->getActiveSheet()->setCellValue('A1', 'Date');
			$this->excel->getActiveSheet()->setCellValue('B1', 'Time');			
			$this->excel->getActiveSheet()->setCellValue('C1', 'Thing');			
			$this->excel->getActiveSheet()->setCellValue('D1', 'Value');


			$style= array(

				 'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		         ),
				 'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '1479B8'),
			        'size'  => 15,
			        'name'  => 'Verdana'
			    ),
			        'fill' => array(
				            'type' => PHPExcel_Style_Fill::FILL_SOLID,
				            'color' => array('rgb' => '66CCFF')
				        ),
			        'borders' => array(
			            'allborders' => array(
			                'style' => PHPExcel_Style_Border::BORDER_THIN,
			                'color' => array('rgb' => '1479B8')
			            )
			        )
			    );
			


			$border = array(
				'borders' => array(
					'outline' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('argb' => '696969'),
					),
				),
			);


			$this->excel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style);
			$this->excel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setWrapText(true);

			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);

			$this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);







			if(isset($post['sel_box'])){

					$ctr=2;

					foreach ($this->result_array['things_zone_result'] as $key => $thing) {	


/*							$exp_things  = explode(',',$post['sel_box']);*/

		

								if(in_array( $thing['name'], 	$post['sel_box'])){					
									

									$swd_history = $this->agresearch->prop_history($this->auth_settings,$thing['key'],'SWC');
									

							
									foreach ($swd_history as $key => $value) {

										$date_ts = date('Y-m-d',strtotime($value['ts']));
										
										
										

										if (($date_ts >= date('Y-m-d',strtotime(trim($date_range_arr[0])))) && ($date_ts <= date('Y-m-d',strtotime(trim($date_range_arr[1]))))){

											$this->excel->getActiveSheet()->setCellValue('A'.$ctr, $date_ts);
											$this->excel->getActiveSheet()->setCellValue('B'.$ctr, date('H:i:s',strtotime($value['ts'])));
											$this->excel->getActiveSheet()->setCellValue('C'.$ctr, $thing['name'] );
											$this->excel->getActiveSheet()->setCellValue('D'.$ctr, $value['value']);											

											$this->excel->getActiveSheet()->getStyle('A'.$ctr)->applyFromArray($border);
											$this->excel->getActiveSheet()->getStyle('B'.$ctr)->applyFromArray($border);
											$this->excel->getActiveSheet()->getStyle('C'.$ctr)->applyFromArray($border);
											$this->excel->getActiveSheet()->getStyle('D'.$ctr)->applyFromArray($border);

											$this->excel->getActiveSheet()->getRowDimension($ctr)->setRowHeight(20);
											$ctr++;

										}
											
									}

								}
							
							}	


							if($ctr==2){



							}

						
							$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


							$filename='Soil_Moisture_'.trim(str_replace('/','_',$date_range_arr[0])).'_'.trim(str_replace('/','_',$date_range_arr[1])).rand(date('mdYHis'), 5).'.xls'; 

							/*header('Content-Type: application/vnd.ms-excel'); 
							header('Content-Disposition: attachment;filename="'.$filename.'"'); 
							header('Cache-Control: max-age=0');*/


							 							
							$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  

							$url = 'files/'.$filename;

							if(read_file($url)) {
							    unlink($url);
							}


							$objWriter->save($url);

							chmod($url, 0777);
								
							if(read_file($url)){
								
								$json_output = json_encode(array('url'=>$url));

							}else{
								$json_output = json_encode(array('error'=>'Unable to create report.'));								
							}


							


			}

		echo $json_output;
	}

		public function get_factor(){

			$post = $this->input->post();


			$selected_thing = array();


			if(isset($post['thing'])){


					foreach ($this->result_array['things_zone_result'] as $key => $value) {

								if($post['thing']==$value['name']){
									$selected_thing[] = $value;
								}
					
				
					}
			}

			if(count('selected_thing')==0){

				$json = json_encode(array('error'=>'Unable to find thing.'));	

			}else{

				$selected_thing['factors'] = IRRIGATION_FACTORS;
				$json = json_encode($selected_thing);	
			}

			echo $json;
			
	}


}?>
