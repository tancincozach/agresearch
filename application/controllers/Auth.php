<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){

		parent::__construct();


		

	}	


	public function index(){

		$this->login();

	}


	public function login(){


		//$posted_values = array('username'=>'zach@m2mzone.co','password'=>'Cloudus3r!)!');
		$posted_values = $this->input->post();

		if($_POST){

			if($posted_values['username']!='' && $posted_values['password']!=''){


			    try {


			    	if(!isset($posted_values['username']) || !isset($posted_values['password'])) {

						throw new Exception('Error: Input required fields.');
			    	}

			    	$auth_settings['userName'] = $posted_values['username'];
			    	$auth_settings['userPass'] = $posted_values['password'];
			    	$auth_settings['_endPoint'] = DEVICE_WISE_API;


					$this->load->library('Rest' ,$auth_settings );
					
				
				    $result = $this->rest->auth_login();

				   

			    	if(isset($result['response']['errorMessages'])){			    
			    		

					    throw new Exception(implode(',',$result['response']['errorMessages']));
					    
					}

					
					$sess = array();

					$sess['logged_in'] 	= 1; 
					$sess['devicewise_response'] 	=  $result['response']['auth']['params'];
					$sess['uname'] 	= $posted_values['username'];
					$sess['pass'] 	= $posted_values['password'];


					$this->session->set_userdata($sess);
					

				   $base64_encode_str = base64_encode(json_encode($sess));	


				   $this->session->set_flashdata('msg', 'Login Successful!.');	


				   set_cookie('sess_user',$base64_encode_str,28800);	

						redirect(base_url().'dashboard');  
			    	
			    } catch (Exception $e) {

						$this->session->set_flashdata('error',$e->getMessage());				 

						redirect(base_url().'login');   		
			    }	

			}else{

				$this->session->set_flashdata('error','Please input Username/Password.');

				redirect(base_url().'login');   		

			}
						 
		}
	    	    


		$this->load->view('login');
	}

	function autologin(){


	}

	public function logout(){

		$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		
		$flash_msg = '<p class="text-success bg-success">You have succesfully logout</p>';

		$this->session->set_flashdata('login_message', $flash_msg); 

		redirect(base_url().'login');

	}	

}
