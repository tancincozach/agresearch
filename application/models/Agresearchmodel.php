<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




class Agresearchmodel extends CI_Model{ 




	/*
		  a method that retrieves the coordinates for the geoference or for the google map api.
		*/
		public function getDeviceCoordinates( $thing_result) {


				$geo_location_info = array();

				foreach ($thing_result as $thing) {

					  
						    $address = array();

						  if(isset($thing['loc']['addr']['streetNumber']) ){
						    array_push($address,$thing['loc']['addr']['streetNumber']);
						  }

						  if(isset($thing['loc']['addr']['street']) ){
						    array_push($address,$thing['loc']['addr']['street']);
						  }

						  if(isset($thing['loc']['addr']['city']) ){
						    array_push($address,$thing['loc']['addr']['city']);
						  } 

						  if(isset($thing['loc']['addr']['state']) ){
						    array_push($address,$thing['loc']['addr']['state']);
						  } 

					 	$geo_location_info [] = array (
													'fulladdress'=> (count($address) > 0 ) ? implode(" ",$address) :"Geolocation is not available.",
													'lat'=>  @$thing['loc']['lat'],
													'lng'=> @$thing['loc']['lng']     
							);

				}

				return $geo_location_info;
		  
		}

		public function segregate_zone_things( $auth_settings){


			 $things_array = array();

			 $things_with_swc_arr = array();

			$this->load->library('Devicewise' ,$auth_settings );			



			$this->devicewise->session_org_switch(DEVICE_WISE_ORG);		

			$ctr=0;

			$white_list = array('zone');

			$things_result = $this->devicewise->thing_list();


			foreach($this->devicewise->thing_list($offset = 0, $limit = 10, $show = array('key','name'), $sort = '+name', $tags = array('zone'), false ,false) as $parent_key=> $thing_array){


					$things_array[]= $this->devicewise->thing_find($thing_array['key']);


					

					if(isset($thing_array['defKey'])){


							$thing_def_array  = $this->devicewise->thing_def_find($thing_array['defKey']);  

							$thing_def_properties = @$thing_def_array['properties'];

							
							unset($thing_def_array);

							 if(isset($thing_def_properties))
							  {
							    if(count($thing_def_properties) > 0 )
							    {      
							       foreach($thing_def_properties as $propKey=>$property)
							       {


							         if(array_key_exists($propKey, $thing_def_properties))
							         {          
							            $prefix = isset($thing_def_properties[$propKey]['prefix']) ? $thing_def_properties[$propKey]['prefix']:$thing_def_properties[$propKey]['name'];


							            if(isset($things_array[$ctr]['properties']) && array_key_exists($propKey, $things_array[$ctr]['properties'])){


							            	$things_array[$ctr]['properties'][$propKey] = array_merge( $things_array[$ctr]['properties'][$propKey], $thing_def_properties[$propKey]);
							            
							            }
					        				
							         }          
							       }
							      
							    }
							  }

					}

						if(array_key_exists('soil_type',$things_array[$ctr]['properties']) || array_key_exists('irrigation_type',$things_array[$ctr]['properties'])){

									$log_list = $this->devicewise->log_list($thing_array['key'],$thing_array['key'].'-publish');

										if( count($log_list) > 0){
												
												if(count($log_obj_result = json_decode($log_list[0]['msg'])) > 0){

													$things_array[$ctr]['properties']['soil_type']['value'] = $log_obj_result->soil_type;
													$things_array[$ctr]['properties']['irrigation_type']['value'] = $log_obj_result->irrigation_type;
													
												}

												
										}

						}

					/*	if(isset($things_array[$ctr]['properties']['swc'])){

							$things_with_swc_arr[$ctr] = $things_array[$ctr]['name'];

						
						}*/
				

					$ctr++;
			}



		 	return  array(	
					 		'things_zone_result'=>$things_array ,
					 		'things_with_swc'=>array_unique($things_with_swc_arr, SORT_REGULAR),
					 		'things_result'=>$things_result,
					 		'things_last_data'=>$things_array[count($things_array)-1]
		 		);


		 	
	}
	

	public function post_factor(){

	}

	public function publish_properties($auth_settings,$posted_values=array()){


		if(count($posted_values) > 0){

			$things_array = array();



			$this->load->library('Devicewise' ,$auth_settings );			

			$this->devicewise->session_org_switch(DEVICE_WISE_ORG);		

			$things_array[]= $this->devicewise->thing_find($posted_values['pod_num']);


			
			$log_post = array();


			foreach ($posted_values as $key => $value) {

				   if($key!='pod_num'){

				   		if($key=='soil_type'  || $key=='irrigation_type' ){

							$log_post[$key] = $value;


				   		}else{

				   			$this->devicewise->property_publish($things_array[0]['key'], $key, $value);	

				   		}				   		
				   }								   
			
			}			

			  $this->devicewise->publish_log($things_array[0]['key'], $log_post);

	

		}
		   
	}

	public function publish_factor($auth_settings,$thing_key,$posted_values=array()){	

			if(count($posted_values) > 0){				

					foreach ($posted_values as $key => $value) {

						$this->devicewise->property_publish($thing_key, $key, $value);				   									  
					}
	
			}
			
	}

	public function publish_pod($auth_settings,$thing_key,$posted_values=array()){

		if(count($posted_values) > 0){

			$things_array = array();

			$this->load->library('Devicewise' ,$auth_settings );			

			$this->devicewise->session_org_switch(DEVICE_WISE_ORG);		

			foreach ($posted_values as $key => $value) {

				$this->devicewise->attr_publish($thing_key, $key, $value);		

			}

			
		}
	}

	public function get_latest_log($log_list= array()){

		if(count($log_list) > 0){

		}

	}

	public function prop_history($auth_settings,$thing_key,$property_key){

		$this->load->library('Devicewise' ,$auth_settings );			

		$return_array  = $this->devicewise->property_history($thing_key, $property_key, 100);	

		return @$return_array['values'];

	}





}