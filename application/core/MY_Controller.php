<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	
	public $logged_in;
 	
 	public $view_data = array(
 			'container'=>'container-fluid', 
 			'view_file'=>'default-content', 
 			'js_file'=>'', //js by page
 			'css_file'=>'', //css by page
 			'data'=>'',
 			'menu_active'=>'dashboard'
 		);

 	public $user_id = 0;





	function __construct()
	{

		
		parent::__construct();
 		


 		date_default_timezone_set('NZ');

		if( !$this->session->userdata('logged_in')){
			//redirect(base_url().'login');
			
				redirect(base_url().'login');
		
		}
		 


	}
 
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */